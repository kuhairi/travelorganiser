package com.example.sandyphan.travelorganiser.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.sandyphan.travelorganiser.Activities.JourneyDetailsActivity;
import com.example.sandyphan.travelorganiser.Activities.MyJourneyActivity;
import com.example.sandyphan.travelorganiser.Objects.Journey;
import com.example.sandyphan.travelorganiser.R;

import java.util.List;

/**
 * Created by sandy.phan on 31/12/16.
 */

public class JourneysAdapter extends RecyclerView.Adapter<JourneysAdapter.MyViewHolder> {

    private Context _context;
    private List<Journey> _journeyList;

    public JourneysAdapter(Context context, List<Journey> journeys)
    {
        this._context = context;
        this._journeyList = journeys;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.journey_card, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Journey journey = _journeyList.get(position);
        holder.journeyName.setText(journey.getName());
        holder.destination.setText("Destinations: " + journey.getDestination());
        holder.journeyCurrency.setText("Journey currency: " + journey.getCurrency());
        holder.startDate.setText("Journey start date: " + journey.getStartDate());
        holder.endDate.setText("Journey end date: " + journey.getEndDate());
        holder.journey = journey;
    }

    @Override
    public int getItemCount() {
        return _journeyList.size();
    }

    public class MyViewHolder extends  RecyclerView.ViewHolder
    {
        private final String TAG = MyViewHolder.class.getSimpleName();

        public TextView journeyName, destination, country, journeyCurrency, startDate, endDate;
        public Journey journey;

        public MyViewHolder(View itemView) {
            super(itemView);

            journeyName = (TextView) itemView.findViewById(R.id.journey_name);
            destination = (TextView) itemView.findViewById(R.id.journey_destination);
            country = (TextView) itemView.findViewById(R.id.journey_country);
            journeyCurrency = (TextView) itemView.findViewById(R.id.journey_currency);
            startDate = (TextView) itemView.findViewById(R.id.journey_start_date);
            endDate = (TextView) itemView.findViewById(R.id.journey_end_date);

            itemView.setOnClickListener(new View.OnClickListener()
            {

                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(v.getContext(), JourneyDetailsActivity.class);
                    Log.d(TAG, "journey.getId() = " + journey.getId());
                    intent.putExtra("JOURNEY_ID", journey.getId());
                    _context.startActivity(intent);
                }
            });
        }
    }
}
