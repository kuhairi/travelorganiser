package com.example.sandyphan.travelorganiser.Adapters;

import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.example.sandyphan.travelorganiser.Objects.TravelObject;
import com.example.sandyphan.travelorganiser.R;

import java.util.List;

/**
 * Created by sandy.phan on 5/1/17.
 */

public class SpinAdapter implements SpinnerAdapter {

    private List<? extends TravelObject> _travelObjects;
    private Context _context;
    private LayoutInflater _inflater = null;

    public SpinAdapter(Context context, List<? extends TravelObject> data)
    {
        this._context = context;
        this._travelObjects = data;
        _inflater = LayoutInflater.from(_context);

    }


    @Override
    public void registerDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public int getCount() {
        return _travelObjects.size();
    }

    @Override
    public Object getItem(int position) {
        return _travelObjects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return _travelObjects.get(position).getId();
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater vi = (LayoutInflater) _context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(android.R.layout.simple_spinner_item, null);
        }

        TextView label = (TextView)convertView;

        label.setPadding(16, 16, 16, 16);
        label.setTextSize(14);

        label.setTextColor(Color.BLACK);
        // Then you can get the current item using the values array (Users array) and the current position
        // You can NOW reference each method you has created in your bean object (User class)
        label.setText(_travelObjects.get(position).getName());

        // And finally return your dynamic (or custom) view for each spinner item
        return label;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean isEmpty() {
        return _travelObjects.isEmpty();
    }

    // And here is when the "chooser" is popped up
    // Normally is the same view, but you can customize it if you want
    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater vi = (LayoutInflater) _context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(android.R.layout.simple_dropdown_item_1line, parent, false);
        }

        TextView label = (TextView)convertView;
        label.setPadding(16, 16, 16, 16);
        label.setTextSize(14);
        label.setTextColor(Color.BLACK);
        label.setText(_travelObjects.get(position).getName());

        return label;
    }
}
