package com.example.sandyphan.travelorganiser.Activities;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.example.sandyphan.travelorganiser.Activities.Base.BaseHomeActivity;
import com.example.sandyphan.travelorganiser.Adapters.JourneysAdapter;
import com.example.sandyphan.travelorganiser.Database.Tables.JourneyTable;
import com.example.sandyphan.travelorganiser.Objects.Contants;
import com.example.sandyphan.travelorganiser.Objects.Journey;
import com.example.sandyphan.travelorganiser.R;

import java.util.List;

public class MyJourneyActivity extends BaseHomeActivity {

    private RecyclerView recyclerView;
    private JourneysAdapter journeysAdapter;
    private List<Journey> journeyList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_journey);
        super.onCreateBaseHomeActivity();

        setToolbarTitle(Contants.MY_JOURNEY_TOOLBAR_TITLE);

        JourneyTable journeyTable = new JourneyTable(this);

        FloatingActionButton addJourneyButton = (FloatingActionButton) findViewById(R.id.add_journey_button);
        recyclerView = (RecyclerView) findViewById(R.id.journey_recycler_view);

        journeyList = journeyTable.getAllJourneys();
        journeysAdapter = new JourneysAdapter(this, journeyList);

        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this, 1);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(journeysAdapter);

        addJourneyButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), NewJourneyActivity.class);
                intent.putExtra("CAME_FROM_ACTIVITY", MyJourneyActivity.class.getSimpleName());
                finish();
                startActivity(intent);
            }
        });
    }
}
