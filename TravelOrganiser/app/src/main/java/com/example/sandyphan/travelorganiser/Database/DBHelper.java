package com.example.sandyphan.travelorganiser.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.sandyphan.travelorganiser.Database.Tables.AccommodationTable;
import com.example.sandyphan.travelorganiser.Database.Tables.CategoryTable;
import com.example.sandyphan.travelorganiser.Database.Tables.CountryTable;
import com.example.sandyphan.travelorganiser.Database.Tables.CurrencyTable;
import com.example.sandyphan.travelorganiser.Database.Tables.DayPlanTable;
import com.example.sandyphan.travelorganiser.Database.Tables.DestinationTable;
import com.example.sandyphan.travelorganiser.Database.Tables.ExpenseTable;
import com.example.sandyphan.travelorganiser.Database.Tables.JourneyTable;
import com.example.sandyphan.travelorganiser.Database.Tables.LuggageCheckItemTable;
import com.example.sandyphan.travelorganiser.Database.Tables.ToDoListTable;
import com.example.sandyphan.travelorganiser.Objects.Category;
import com.example.sandyphan.travelorganiser.Objects.Destination;

/**
 * Created by Sandy.Phan on 09/12/2016.
 */

public class DBHelper extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "travelorganiser.db";
    private static final String TAG = DBHelper.class.getSimpleName().toString();

    private static DBHelper instance;

    public static synchronized DBHelper getHelper(Context context)
    {
        if (instance == null)
        {
            instance = new DBHelper(context);
        }
        return instance;
    }

    public DBHelper(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onOpen(SQLiteDatabase db)
    {
        super.onOpen(db);
    }


    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db)
    {
        db.execSQL(CategoryTable.createTable());
        db.execSQL(JourneyTable.createTable());
        db.execSQL(ExpenseTable.createTable());
        db.execSQL(CountryTable.createTable());
        db.execSQL(CurrencyTable.createTable());
        db.execSQL(AccommodationTable.createTable());
        db.execSQL(DayPlanTable.createTable());
        db.execSQL(DestinationTable.createTable());
        db.execSQL(ToDoListTable.createTable());
        db.execSQL(LuggageCheckItemTable.createTable());
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {

    }
}
