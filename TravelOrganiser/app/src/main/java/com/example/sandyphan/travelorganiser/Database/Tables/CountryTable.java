package com.example.sandyphan.travelorganiser.Database.Tables;

import android.content.ContentValues;
import android.content.Context;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.util.Log;

import com.example.sandyphan.travelorganiser.Database.DBHelper;
import com.example.sandyphan.travelorganiser.Objects.Country;
import com.example.sandyphan.travelorganiser.Objects.Currency;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sandy.Phan on 09/12/2016.
 */

public class CountryTable extends BaseTable{
    public static final String TAG = CountryTable.class.getSimpleName();

    private static final String TABLE_NAME = "country";
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";

    public CountryTable(Context context)
    {
        super();
        this.mContext = context;
        this.dbHelper = DBHelper.getHelper(mContext);

        populateTable();
    }

    public static String createTable()
    {
        return "CREATE TABLE " + TABLE_NAME + "(" +
                KEY_ID + PRIMARY_KEY + COMMA_SEP +
                KEY_NAME + TEXT_TYPE + ");";
    }

    public void populateTable()
    {
        AssetManager assetManager = mContext.getAssets();

        if (!checkTableIsEmpty(TABLE_NAME))
        {
            return;
        }

        try
        {
            InputStream csvStream = assetManager.open("currency_list.csv");
            BufferedReader reader = new BufferedReader(new InputStreamReader(csvStream));

            String line;

            while ((line = reader.readLine()) != null)
            {
                String[] rowData = line.split(",");
                String currencyName = rowData[0];

                int id = insert(currencyName);
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();;
        }
    }

    public int insert(String currencyName)
    {
        ContentValues values = new ContentValues();
//        SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();
        openDatabase();

        values.put(KEY_NAME, currencyName);

        int id = (int) database.insert(TABLE_NAME, null, values);

        closeDatabase();

        return id;
    }

    public List<Country> getAllCountries()
    {
        List<Country> currencies = new ArrayList<>();
        openDatabase();

        String selectQuery = "SELECT * FROM " + TABLE_NAME;

        Cursor cursor = database.rawQuery(selectQuery, null);

        if (cursor != null && cursor.moveToFirst())
        {
            do
            {
                Country country = new Country();

                country.setId(cursor.getInt(cursor.getColumnIndex(KEY_ID)));
                country.setName(cursor.getString(cursor.getColumnIndex(KEY_NAME)));

                currencies.add(country);

            } while (cursor.moveToNext());
        }

        cursor.close();
        closeDatabase();

        return currencies;
    }
    public Country getCountryByName(String name)
    {
        Country country = new Country();
        openDatabase();

        String selectQuery = "SELECT * FROM " + TABLE_NAME + " WHERE " + KEY_NAME + " LIKE '%" + name + "%'";
        Log.d(TAG, selectQuery);


        Cursor cursor = database.rawQuery(selectQuery, null);

        if (cursor != null && cursor.moveToFirst())
        {
            do
            {
                country = new Country();

                country.setId(cursor.getInt(cursor.getColumnIndex(KEY_ID)));
                country.setName(cursor.getString(cursor.getColumnIndex(KEY_NAME)));


            } while (cursor.moveToNext());
        }

        cursor.close();
        closeDatabase();

        return country;
    }

}
