package com.example.sandyphan.travelorganiser.Activities;

import android.content.Intent;
import android.content.res.AssetManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.example.sandyphan.travelorganiser.Activities.Base.BaseDetailsActivity;
import com.example.sandyphan.travelorganiser.Adapters.AccommodationsAdapter;
import com.example.sandyphan.travelorganiser.Adapters.JourneysAdapter;
import com.example.sandyphan.travelorganiser.Database.Tables.AccommodationTable;
import com.example.sandyphan.travelorganiser.Database.Tables.JourneyTable;
import com.example.sandyphan.travelorganiser.Objects.Accomodation;
import com.example.sandyphan.travelorganiser.Objects.Contants;
import com.example.sandyphan.travelorganiser.R;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

public class AccommodationActivity extends BaseDetailsActivity {

    private final static String TAG = AccommodationActivity.class.getSimpleName();

    AccommodationTable accommodationTable;

    private RecyclerView recyclerView;
    private AccommodationsAdapter accommodationsAdapter;
    private List<Accomodation> accomodationList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accommodation);
        onCreateBaseDetailsActivity();
        getJourneyIdFromExtras();

        setToolbarTitle(Contants.ACCOMMODATION_TOOLBAR_TITLE);

        accommodationTable = new AccommodationTable(this);

//        loadAccommodationsFromCSV();

        FloatingActionButton addAccomodationButton = (FloatingActionButton) findViewById(R.id.add_accommodation_button);
        recyclerView = (RecyclerView) findViewById(R.id.journey_recycler_view);

        accomodationList = accommodationTable.getAllAccommodationsByJourney(journeyId);
        accommodationsAdapter = new AccommodationsAdapter(this, accomodationList);

        recyclerView = (RecyclerView) findViewById(R.id.accommodation_recycler_view);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this, 1);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(accommodationsAdapter);


        addAccomodationButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), NewAccommodationActivity.class);
                intent.putExtra("JOURNEY_ID", journeyId);
                finish();
                startActivity(intent);
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToJourneyDetailsActivity();
            }
        });
    }

    @Override
    public void onBackPressed()
    {
        goToJourneyDetailsActivity();
    }

    public void goToJourneyDetailsActivity()
    {
        Intent intent = new Intent(this, JourneyDetailsActivity.class);
        intent.putExtra("JOURNEY_ID", journeyId);
        finish();
        startActivity(intent);
    }

    private void getJourneyIdFromExtras()
    {
        Bundle extras = getIntent().getExtras();
        if (extras != null)
        {
            journeyId = extras.getInt("JOURNEY_ID");
        }
    }

//    public void loadAccommodationsFromCSV()
//    {
//        AssetManager assetManager = this.getAssets();
//
//        try
//        {
//            InputStream csvStream = assetManager.open("accommodations.csv");
//            BufferedReader reader = new BufferedReader(new InputStreamReader(csvStream));
//
//            String line;
//
//            while ((line = reader.readLine()) != null)
//            {
//                String[] rowData = line.split(",");
//                Log.d(TAG, "rowdata.length: " + rowData.length);
//                String name = rowData[0];
//                String address = rowData[1];
//                String checkInDate = rowData[2];
//                String checkOutDate = rowData[3];
//
//                accommodationTable.insert(name, address, checkInDate, checkOutDate, journeyId);
//            }
//        }
//        catch (IOException e)
//        {
//            e.printStackTrace();;
//        }
//    }
}
