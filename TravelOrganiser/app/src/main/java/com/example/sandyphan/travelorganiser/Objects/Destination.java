package com.example.sandyphan.travelorganiser.Objects;

/**
 * Created by Sandy.Phan on 09/12/2016.
 */

public class Destination extends TravelObject {
    public static final String TAG = Destination.class.getSimpleName();

    private String _address;
    private String _openingHours;
    private String _howToGetThere;
    private double _entryCost;
    private String _url;
    private String _notes;
    private int _journeyId;
    private int _dayPlanId;

    public Destination() { super(); }

    public String getUrl() {
        return _url;
    }

    public void setUrl(String url) {
        this._url = url;
    }

    public String getAddress() {
        return _address;
    }

    public void setAddress(String _address) {
        this._address = _address;
    }

    public String getOpeningHours() {
        return _openingHours;
    }

    public void setOpeningHours(String _openingHours) {
        this._openingHours = _openingHours;
    }

    public double getEntryCost() {
        return _entryCost;
    }

    public void setEntryCost(double _cost) {
        this._entryCost = _cost;
    }

    public int getJourneyId() {
        return _journeyId;
    }

    public void setJourneyId(int _journeyId) {
        this._journeyId = _journeyId;
    }

    public int getDayPlanId() {
        return _dayPlanId;
    }

    public void setDayPlanId(int _dayPlanId) {
        this._dayPlanId = _dayPlanId;
    }

    public String getHowToGetThere() {
        return _howToGetThere;
    }

    public void setHowToGetThere(String _howToGetThere) {
        this._howToGetThere = _howToGetThere;
    }

    public String getNotes() {
        return _notes;
    }

    public void setNotes(String _notes) {
        this._notes = _notes;
    }
}
