package com.example.sandyphan.travelorganiser.Objects;

/**
 * Created by Sandy.Phan on 09/12/2016.
 */

public class Expense extends TravelObject {
    public static final String TAG = Expense.class.getSimpleName();
    private double _amount;
    private String _category;
    private String _currency;
    private String _datetime;
    private int _journeyId;

    // Empty Constructor
    public Expense() { super(); }

    public double getAmount() {
        return _amount;
    }

    public void setAmount(double _amount) {
        this._amount = _amount;
    }

    public String getCategory() {
        return _category;
    }

    public void setCategory(String _category) {
        this._category = _category;
    }

    public String getCurrency() {
        return _currency;
    }

    public void setCurrency(String _currency) {
        this._currency = _currency;
    }

    public String getDatetime() {
        return _datetime;
    }

    public void setDatetime(String _datetime) {
        this._datetime = _datetime;
    }

    public int getJourneyId() {
        return _journeyId;
    }

    public void setJourneyId(int _journeyId) {
        this._journeyId = _journeyId;
    }
}
