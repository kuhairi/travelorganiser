package com.example.sandyphan.travelorganiser.Objects;

/**
 * Created by Sandy.Phan on 09/12/2016.
 */

public class Journey extends TravelObject {
    public static final String TAG = Journey.class.getSimpleName();

    private String _destination;
    private String _startDate;
    private String _endDate;
    private int _numberOfPeople;
    private double _nativeTotalCost;
    private double _exchangeRate;
    private String _currency;
    private double _averageCostPerPerson;
    private double _foreignTotalCost;

    public Journey() { super(); }

    public String getDestination() {
        return _destination;
    }

    public void setDestination(String _description) {
        this._destination = _description;
    }

    public String getStartDate() {
        return _startDate;
    }

    public void setStartDate(String _startDate) {
        this._startDate = _startDate;
    }

    public String getEndDate() {
        return _endDate;
    }

    public void setEndDate(String _endDate) {
        this._endDate = _endDate;
    }

    public int getNumberOfPeople() {
        return _numberOfPeople;
    }

    public void setNumberOfPeople(int _numberOfPeople) {
        this._numberOfPeople = _numberOfPeople;
    }

    public double getNativeTotalCost() {
        return _nativeTotalCost;
    }

    public void setNativeTotalCost(double _totalCost) {
        this._nativeTotalCost = _totalCost;
        this._averageCostPerPerson = _totalCost / _numberOfPeople;
    }

    public String getCurrency() {
        return _currency;
    }

    public void setCurrency(String _currency) {
        this._currency = _currency;
    }

    public double getAverageCostPerPerson() {
        return _averageCostPerPerson;
    }


    public double getExchangeRate() {
        return _exchangeRate;
    }

    public void setExchangeRate(double _exchangeRate) {
        this._exchangeRate = _exchangeRate;
    }

    public double getForeignTotalCost() {
        return _foreignTotalCost;
    }

    public void setForeignTotalCost(double _foreginTotalCost) {
        this._foreignTotalCost = _foreginTotalCost;
    }
}