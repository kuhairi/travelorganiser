package com.example.sandyphan.travelorganiser.Database.Tables;

import android.content.ContentValues;
import android.content.Context;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.sandyphan.travelorganiser.Database.DBHelper;
import com.example.sandyphan.travelorganiser.Database.DatabaseManager;
import com.example.sandyphan.travelorganiser.Objects.Currency;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sandy.Phan on 09/12/2016.
 */

public class CurrencyTable extends BaseTable{
    public static final String TAG = CurrencyTable.class.getSimpleName();

    private static final String TABLE_NAME = "currency";
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";

    public CurrencyTable(Context context)
    {
        super();
        this.mContext = context;
        this.dbHelper = DBHelper.getHelper(mContext);

        populateTable();
    }

    public static String createTable()
    {
        return "CREATE TABLE " + TABLE_NAME + "(" +
                KEY_ID + PRIMARY_KEY + COMMA_SEP +
                KEY_NAME + TEXT_TYPE + ");";
    }

    public void populateTable()
    {
        AssetManager assetManager = mContext.getAssets();

        if (!checkTableIsEmpty(TABLE_NAME))
        {
            return;
        }

        try
        {
            InputStream csvStream = assetManager.open("currency_list.csv");
            BufferedReader reader = new BufferedReader(new InputStreamReader(csvStream));

            String line;

            while ((line = reader.readLine()) != null)
            {
                String[] rowData = line.split(",");
                String currencyName = rowData[1];

                int id = insert(currencyName);
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();;
        }
    }

    public int insert(String currencyName)
    {
        ContentValues values = new ContentValues();
//        SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();
        openDatabase();

        values.put(KEY_NAME, currencyName);

        int id = (int) database.insert(TABLE_NAME, null, values);

        closeDatabase();

        return id;
    }

    public List<Currency> getAllCurrencies()
    {
        List<Currency> currencies = new ArrayList<>();
        openDatabase();

        String selectQuery = "SELECT * FROM " + TABLE_NAME;

        Cursor cursor = database.rawQuery(selectQuery, null);

        if (cursor != null && cursor.moveToFirst())
        {
            do
            {
                Currency currency = new Currency();

                currency.setId(cursor.getInt(cursor.getColumnIndex(KEY_ID)));
                currency.setName(cursor.getString(cursor.getColumnIndex(KEY_NAME)));

                currencies.add(currency);

            } while (cursor.moveToNext());
        }

        cursor.close();
        closeDatabase();

        return currencies;
    }

    public Currency getCurrencyByName(String name)
    {
        Currency currency = new Currency();
        openDatabase();

        String selectQuery = "SELECT * FROM " + TABLE_NAME + " WHERE " + KEY_NAME + " LIKE '%" + name + "%'";
        Log.d(TAG, selectQuery);


        Cursor cursor = database.rawQuery(selectQuery, null);

        if (cursor != null && cursor.moveToFirst())
        {
            do
            {
                currency = new Currency();

                currency.setId(cursor.getInt(cursor.getColumnIndex(KEY_ID)));
                currency.setName(cursor.getString(cursor.getColumnIndex(KEY_NAME)));


            } while (cursor.moveToNext());
        }

        cursor.close();
        closeDatabase();

        return currency;
    }

    public Currency getCurrencyById(int id)
    {
        Currency currency = new Currency();
        openDatabase();

        String selectQuery = "SELECT * FROM " + TABLE_NAME + " WHERE " + KEY_ID + " = " + id;
        Log.d(TAG, selectQuery);

        Cursor cursor = database.rawQuery(selectQuery, null);

        if (cursor != null && cursor.moveToFirst())
        {
            do
            {
                currency = new Currency();

                currency.setId(cursor.getInt(cursor.getColumnIndex(KEY_ID)));
                currency.setName(cursor.getString(cursor.getColumnIndex(KEY_NAME)));


            } while (cursor.moveToNext());
        }

        cursor.close();
        closeDatabase();

        return currency;
    }
}
