package com.example.sandyphan.travelorganiser.Database.Tables;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.example.sandyphan.travelorganiser.Database.DBHelper;
import com.example.sandyphan.travelorganiser.Objects.Category;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sandy.Phan on 09/12/2016.
 */

public class CategoryTable extends BaseTable {
    public static final String TAG = CategoryTable.class.getSimpleName();

    private static final String TABLE_NAME = "category";
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";

    private static final String[] categories = {"Food", "Mobiles/Internet", "Entry Fees", "Accommodations",
        "Flights", "Transportation", "Gifts", "Miscellaneous"};

    public CategoryTable(Context context) {
        super();
        this.mContext = context;
        this.dbHelper = DBHelper.getHelper(mContext);

        populateTable();
    }


    public static String createTable()
    {
        return "CREATE TABLE " + TABLE_NAME + "(" +
                KEY_ID + PRIMARY_KEY + COMMA_SEP +
                KEY_NAME + TEXT_TYPE + ");";
    }

    public void populateTable()
    {
        if (!checkTableIsEmpty(TABLE_NAME))
        {
            return;
        }

        for (int i = 0; i < categories.length; ++i)
        {
            insert(categories[i]);
        }
    }

    public List<Category> getAllCategories()
    {
        String selectQuery = "SELECT * FROM " + TABLE_NAME;
        List<Category> categories = new ArrayList<>();

        openDatabase();

        Log.e(TAG, selectQuery);

        Cursor cursor = database.rawQuery(selectQuery, null);

        if (cursor != null && cursor.moveToFirst())
        {
            do
           {
                Category category = new Category();
                category.setId(cursor.getInt(cursor.getColumnIndex(KEY_ID)));
                category.setName(cursor.getString(cursor.getColumnIndex(KEY_NAME)));

                categories.add(category);
            } while (cursor.moveToNext());
        }

        cursor.close();

        closeDatabase();

        return categories;
    }

    private int insert(String name)
    {

        openDatabase();
//        SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_NAME, name);

        int categoryId = (int) database.insert(TABLE_NAME, null, values);

        closeDatabase();
//        DatabaseManager.getInstance().closeDatabase();

        return categoryId;
    }

}
