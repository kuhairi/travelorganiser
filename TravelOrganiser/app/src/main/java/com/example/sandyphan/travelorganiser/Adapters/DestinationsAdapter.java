package com.example.sandyphan.travelorganiser.Adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.sandyphan.travelorganiser.Objects.Accomodation;
import com.example.sandyphan.travelorganiser.Objects.Destination;
import com.example.sandyphan.travelorganiser.R;

import java.util.List;

/**
 * Created by sandy.phan on 31/12/16.
 */

public class DestinationsAdapter extends RecyclerView.Adapter<DestinationsAdapter.MyViewHolder> {

    private Context _context;
    private List<Destination> _destinationList;

    public DestinationsAdapter(Context context, List<Destination> destinations)
    {
        this._context = context;
        this._destinationList = destinations;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.destination_card, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Destination destination = _destinationList.get(position);
        holder.destinationName.setText(destination.getName());
        holder.address.setText(destination.getAddress());
        holder.openingHours.setText(destination.getOpeningHours());
        holder.entryCost.setText(Double.toString(destination.getEntryCost()));
        holder.notes.setText(destination.getNotes());
    }

    @Override
    public int getItemCount() {
        return _destinationList.size();
    }

    public class MyViewHolder extends  RecyclerView.ViewHolder
    {
        private final String TAG = MyViewHolder.class.getSimpleName();

        public TextView destinationName, address, openingHours, entryCost, notes;
        public Button getDirectionButton, getUrlButton;

        public MyViewHolder(View itemView) {
            super(itemView);

            destinationName = (TextView) itemView.findViewById(R.id.destination_name);
            address = (TextView) itemView.findViewById(R.id.destination_address_text_view);
            openingHours = (TextView) itemView.findViewById(R.id.destination_opening_hours_text_view);
            entryCost = (TextView) itemView.findViewById(R.id.destination_entry_cost_text_view);
            notes = (TextView) itemView.findViewById(R.id.destination_notes_text_view);
            getDirectionButton = (Button) itemView.findViewById(R.id.getDirectionButton);
            getUrlButton = (Button) itemView.findViewById(R.id.getUrlButton);

            itemView.setOnClickListener(new View.OnClickListener()
            {

                @Override
                public void onClick(View v) {
//                    Intent intent = new Intent(v.getContext(), AccommodationActivity.class);
//                    Log.d(TAG, "accomodation.getId() = " + accomodation.getId());
//                    intent.putExtra("ACCOMODATION_ID", accomodation.getId());
//                    _context.startActivity(intent);
                }
            });

            getDirectionButton.setOnClickListener(new View.OnClickListener()
            {

                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("google.navigation:q=" + address.getText().toString()));
                    Log.d(TAG, "google.navigation:q=" + address.getText().toString());
                    _context.startActivity(intent);
                }
            });
        }
    }
}
