package com.example.sandyphan.travelorganiser.Objects;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sandy.Phan on 09/12/2016.
 */

public class DayPlan extends TravelObject {
    public static String TAG = DayPlan.class.getSimpleName();

    private String _date;
    private List<Destination> _destinations;
    private int _journeyId;

    public DayPlan() { super(); }

    public String getDate() {
        return _date;
    }

    public void setDate(String _date) {
        this._date = _date;
    }

    public List<Destination> getDestinations() {
        return _destinations;
    }

    public void setDestinations(List<Destination> _destinations) {
        this._destinations = _destinations;
    }

    public int getJourneyId() {
        return _journeyId;
    }

    public void setJourneyId(int _journeyId) {
        this._journeyId = _journeyId;
    }
}
