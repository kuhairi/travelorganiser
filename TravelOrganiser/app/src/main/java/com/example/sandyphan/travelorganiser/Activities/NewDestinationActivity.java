package com.example.sandyphan.travelorganiser.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.sandyphan.travelorganiser.Activities.Base.BaseFormActivity;
import com.example.sandyphan.travelorganiser.Database.Tables.DestinationTable;
import com.example.sandyphan.travelorganiser.Objects.Contants;
import com.example.sandyphan.travelorganiser.Objects.Destination;
import com.example.sandyphan.travelorganiser.R;

public class NewDestinationActivity extends BaseFormActivity {

    private final static String TAG = NewDestinationActivity.class.getSimpleName();

    private int dayPlanId;
    private String originalActivity;

    private DestinationTable destinationTable;

    private TextInputLayout nameInputLayout;
    private TextInputLayout destinationAddressInputLayout;
    private TextInputLayout entryCostInputLayout;
    private TextInputLayout openingHoursInputLayout;

    private EditText nameTextBox;
    private EditText openingHoursTextBox;
    private EditText addressTextBox;
    private EditText howTOGetThereTextBox;
    private EditText entryCostTextBox;
    private EditText urlTextBox;
    private EditText noteTextBox;

    private Button addNewDestinationButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_destination);
        onCreateBaseFormActivity(false);
        setToolbarTitle(Contants.NEW_DESTINATION_TOOLBAR_TITLE);

        getExtras();

        destinationTable = new DestinationTable(this);

        nameInputLayout = (TextInputLayout) findViewById(R.id.nameInputLayout);
        destinationAddressInputLayout = (TextInputLayout) findViewById(R.id.destinationAddressInputLayout);
        entryCostInputLayout = (TextInputLayout) findViewById(R.id.entryCostInputLayout);
        openingHoursInputLayout = (TextInputLayout) findViewById(R.id.openingHoursInputLayout);

        nameTextBox = (EditText) findViewById(R.id.destinationNameTextBox);
        openingHoursTextBox = (EditText) findViewById(R.id.openingHoursTextBox);
        addressTextBox = (EditText) findViewById(R.id.destinationAddressTextBox);
        howTOGetThereTextBox = (EditText) findViewById(R.id.howToGetThereTextBox);
        entryCostTextBox = (EditText) findViewById(R.id.entryCostTextBox);
        urlTextBox = (EditText) findViewById(R.id.urlTextBox);
        noteTextBox = (EditText) findViewById(R.id.noteTextBox);

        addNewDestinationButton = (Button) findViewById(R.id.addDestinationButton);
        addNewDestinationButton.setOnClickListener(new OnAddDestinationButtonClicked());
    }

    public void onBackPressed()
    {
        Intent intent;
        intent = new Intent(this, DestinationsActivity.class);
        if (originalActivity.equals(JourneyPlanActivity.class.getSimpleName()))
        {

            intent.putExtra("DAY_PLAN_ID", dayPlanId);
        }
        intent.putExtra("JOURNEY_ID", journeyId);
        intent.putExtra("CAME_FROM_ACTIVITY", originalActivity);
        finish();
        startActivity(intent);
    }

    private void getExtras()
    {
        Bundle extras = getIntent().getExtras();
        if (extras != null)
        {
            journeyId = extras.getInt("JOURNEY_ID");
            dayPlanId = extras.getInt("DAY_PLAN_ID");
            originalActivity = extras.getString("CAME_FROM_ACTIVITY");
        }
    }

    private boolean validateRequiredFields()
    {
        boolean isValid = true;

        if (nameTextBox.getText().toString().trim().equals(""))
        {
            nameInputLayout.setError("Name is required");
            isValid = false;
        }

        if (addressTextBox.getText().toString().trim().equals(""))
        {
            destinationAddressInputLayout.setError("Address is required");
            isValid = false;
        }

        if (entryCostTextBox.getText().toString().trim().equals(""))
        {
            entryCostInputLayout.setError("Entry Cost is required");
            isValid = false;
        }

        if (openingHoursTextBox.getText().toString().trim().equals(""))
        {
            openingHoursInputLayout.setError("Opening Hours is required");
            isValid = false;
        }

        return isValid;
    }

    private class OnAddDestinationButtonClicked implements View.OnClickListener
    {

        @Override
        public void onClick(View v) {
            if (validateRequiredFields())
            {
                String name = nameTextBox.getText().toString();
                String openingHours = openingHoursTextBox.getText().toString();
                String address = addressTextBox.getText().toString();
                String howToGetThere = howTOGetThereTextBox.getText().toString();
                double entryCost = Double.parseDouble(entryCostTextBox.getText().toString());
                String url = urlTextBox.getText().toString();
                String note = noteTextBox.getText().toString();

                destinationTable.insert(name, openingHours, address, howToGetThere,
                        entryCost, url, note, journeyId, dayPlanId);

                Intent intent = new Intent(NewDestinationActivity.this, DestinationsActivity.class);
                intent.putExtra("JOURNEY_ID", journeyId);
                intent.putExtra("DAY_PLAN_ID", dayPlanId);
                intent.putExtra("CAME_FROM_ACTIVITY", originalActivity);
                finish();
                startActivity(intent);
            }

        }
    }
}
