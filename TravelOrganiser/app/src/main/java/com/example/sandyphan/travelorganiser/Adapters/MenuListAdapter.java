package com.example.sandyphan.travelorganiser.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.sandyphan.travelorganiser.R;

/**
 * Created by sandy.phan on 12/2/17.
 */

public class MenuListAdapter extends ArrayAdapter<String> {

    private Context _context;
    private String[] _menuList;


    public MenuListAdapter(Context context, String[] menuList)
    {
        super(context, R.layout.list_menu_items, menuList);
        this._context = context;
        this._menuList = menuList;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) _context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.list_menu_items, parent, false);
        TextView textView = (TextView) rowView.findViewById(R.id.details_menu_item);
        ImageView iconView = (ImageView) rowView.findViewById(R.id.menu_icon);
        textView.setText(_menuList[position]);

        switch (_menuList[position])
        {
            case "Accommodations":
                iconView.setImageResource(R.drawable.hotel);
                break;
            case "Expenses":
                iconView.setImageResource(R.drawable.cash);
                break;
            case "Journey Plan":
                iconView.setImageResource(R.drawable.calendar);
                break;
            case "Destinations":
                iconView.setImageResource(R.drawable.map_marker);
                break;
            case "Check List":
                iconView.setImageResource(R.drawable.checkbox_multiple_marked);
                break;
            case "Must Have Foods":
                iconView.setImageResource(R.drawable.food_fork_drink);
                break;
            default:
                break;
        }

        return rowView;
    }
}
