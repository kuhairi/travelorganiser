package com.example.sandyphan.travelorganiser.Activities;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.sandyphan.travelorganiser.Activities.Base.BaseDetailsActivity;
import com.example.sandyphan.travelorganiser.Adapters.MenuListAdapter;
import com.example.sandyphan.travelorganiser.Database.Tables.JourneyTable;
import com.example.sandyphan.travelorganiser.Objects.Journey;
import com.example.sandyphan.travelorganiser.R;

public class JourneyDetailsActivity extends BaseDetailsActivity {

    private final static String TAG = JourneyDetailsActivity.class.getSimpleName();

    private JourneyTable journeyTable;
    private String[] detailsMenuList;

    private ListView detailsMenuListView;
    private MenuListAdapter menuListAdapter;

    private TextView destinationTextView;
    private TextView exchangeRateTextView;
    private TextView totalCostForeignTextView;
    private TextView totalCostNativeTextView;
    private TextView numberOfPeopleTextView;
    private TextView averageCostTextView;
    private TextView startDateTextView;
    private TextView endDateTextView;
    private TextView foreignCurrencyNameTextView;
    private TextView nativeCurrencyNameTextView;

    private FloatingActionButton editButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_journey_details);
        onCreateBaseDetailsActivity();

        journeyTable = new JourneyTable(this);
        detailsMenuList = getResources().getStringArray(R.array.details_menu_list);

        getJourneyIdFromExtras();

        destinationTextView = (TextView) findViewById(R.id.destinationTextView);
        exchangeRateTextView = (TextView) findViewById(R.id.exchangeRateTextView);
        totalCostForeignTextView = (TextView) findViewById(R.id.totalCostForeignCurrencyTextView);
        totalCostNativeTextView = (TextView) findViewById(R.id.totalCostNativeCurrencyTextView);
        numberOfPeopleTextView = (TextView) findViewById(R.id.numberOfPeopleTextView);
        averageCostTextView = (TextView) findViewById(R.id.averageCostTextView);
        startDateTextView = (TextView) findViewById(R.id.startDateTextView);
        endDateTextView = (TextView) findViewById(R.id.endDateTextView);
        foreignCurrencyNameTextView = (TextView) findViewById(R.id.foreignCurrencyNameTextView);
        nativeCurrencyNameTextView = (TextView) findViewById(R.id.nativeCurrencyNameTextView);

        detailsMenuListView = (ListView) findViewById(R.id.details_menu_list_view);
        menuListAdapter = new MenuListAdapter(this, detailsMenuList);

        editButton = (FloatingActionButton) findViewById(R.id.edit_journey_button);

        Journey journey = journeyTable.getJourneyById(journeyId);
        destinationTextView.setText(journey.getDestination());
        exchangeRateTextView.setText(String.format("%.4f", journey.getExchangeRate()));
        totalCostForeignTextView.setText(Double.toString(journey.getForeignTotalCost()));
        numberOfPeopleTextView.setText(Integer.toString(journey.getNumberOfPeople()));
        startDateTextView.setText(journey.getStartDate());
        endDateTextView.setText(journey.getEndDate());
        foreignCurrencyNameTextView.setText("(" + journey.getCurrency() + ")");
        nativeCurrencyNameTextView.setText("(AUD)");

        double totalCostNative = journey.getNativeTotalCost();
        totalCostNative += convertTotalFromForeignCurrency(journey.getForeignTotalCost(), journey.getExchangeRate());

        totalCostNativeTextView.setText(
                String.format("%.2f", totalCostNative));

        averageCostTextView.setText(String.format("%.2f", totalCostNative / 2));

        detailsMenuListView.setAdapter(menuListAdapter);
        detailsMenuListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position)
                {
                    case 0:
                        openMyAccommodationActivity(view);
                        break;
                    case 1:
                        openMyExpenseActivity(view);
                        break;
                    case 2:
                        openJourneyPlanActivity(view);
                        break;
                    case 3:
                        openDestinationActivity(view);
                        break;
                    case 4:
                        openCheckListActivity(view);
                        break;
                    default:
                        break;
                }
            }
        });


        setToolbarTitle(journey.getName());

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goBack();
            }
        });
        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openJourneyForm();
            }
        });

    }

    public void openMyJourneyDetailsActivity(View view)
    {
        Intent intent = new Intent(this, JourneyDetailsActivity.class);
        intent.putExtra("JOURNEY_ID", journeyId);
        startActivity(intent);
    }

    public void openMyExpenseActivity(View view)
    {
        Intent intent = new Intent(this, MyExpensesActivity.class);
        Log.d(TAG, "journeyId: " + journeyId);
        intent.putExtra("JOURNEY_ID", journeyId);
        startActivity(intent);
    }

    public void openMyAccommodationActivity(View view)
    {
        Intent intent = new Intent(this, AccommodationActivity.class);
        Log.d(TAG, "journeyId: " + journeyId);
        intent.putExtra("JOURNEY_ID", journeyId);
        startActivity(intent);
    }

    public void openJourneyPlanActivity(View view)
    {
        Intent intent = new Intent(this, JourneyPlanActivity.class);
        Log.d(TAG, "journeyId: " + journeyId);
        intent.putExtra("JOURNEY_ID", journeyId);
        startActivity(intent);
    }

    public void openDestinationActivity(View view)
    {
        Intent intent = new Intent(this, DestinationsActivity.class);
        intent.putExtra("JOURNEY_ID", journeyId);
        intent.putExtra("CAME_FROM_ACTIVITY", JourneyDetailsActivity.class.getSimpleName());
        startActivity(intent);
    }

    public void openCheckListActivity(View view)
    {
        Intent intent = new Intent(this, CheckListActivity.class);
        Log.d(TAG, "journeyId: " + journeyId);
        intent.putExtra("JOURNEY_ID", journeyId);
        startActivity(intent);
    }

    @Override
    public void onBackPressed()
    {
        goBack();
    }

    private void openJourneyForm()
    {
        Intent intent = new Intent(this, NewJourneyActivity.class);
        intent.putExtra("JOURNEY_ID", journeyId);
        intent.putExtra("CAME_FROM_ACTIVITY", JourneyDetailsActivity.class.getSimpleName());
        startActivity(intent);
    }

    private void getJourneyIdFromExtras()
    {
        Bundle extras = getIntent().getExtras();
        if (extras != null)
        {
            journeyId = extras.getInt("JOURNEY_ID");
        }
    }

    private void goBack()
    {
        Intent intent = new Intent(this, MyJourneyActivity.class);
        finish();
        startActivity(intent);
    }

    private double convertTotalFromForeignCurrency(double foreignCost, double exchangeRate)
    {
        return foreignCost / exchangeRate;
    }
}
