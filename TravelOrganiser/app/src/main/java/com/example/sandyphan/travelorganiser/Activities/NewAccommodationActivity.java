package com.example.sandyphan.travelorganiser.Activities;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;

import com.example.sandyphan.travelorganiser.Activities.Base.BaseFormActivity;
import com.example.sandyphan.travelorganiser.Database.Tables.AccommodationTable;
import com.example.sandyphan.travelorganiser.Database.Tables.JourneyTable;
import com.example.sandyphan.travelorganiser.Objects.Contants;
import com.example.sandyphan.travelorganiser.Objects.Journey;
import com.example.sandyphan.travelorganiser.R;

import org.w3c.dom.Text;

import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class NewAccommodationActivity extends BaseFormActivity {

    private static final String TAG = NewAccommodationActivity.class.getSimpleName();

    private AccommodationTable accommodationTable;
    private JourneyTable journeyTable;

    private DatePickerDialog datePickerDialog;
    private Calendar calendar;
    private int year, month, date;

    private TextInputLayout nameInputLayout;
    private TextInputLayout addressInputLayout;
    private TextInputLayout checkInDateInputLayout;
    private TextInputLayout checkInTimeInputLayout;
    private TextInputLayout checkOutDateInputLayout;
    private TextInputLayout checkOutTimeInputLayout;

    private Button addAccommodationButton;
    private EditText nameTextBox;
    private EditText addressTextBox;
    private EditText checkInDateTextBox;
    private EditText checkOutDateTextBox;
    private EditText checkInTimeTextBox;
    private EditText checkOutTimeTextBox;
    private EditText notesTextBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_accommodation);
        onCreateBaseFormActivity(false);
        setToolbarTitle(Contants.NEW_ACCOMMODATION_TOOLBAR_TITLE);

        getJourneyIdFromExtras();

        accommodationTable = new AccommodationTable(this);
        journeyTable = new JourneyTable(this);

        calendar = Calendar.getInstance();

        nameInputLayout = (TextInputLayout) findViewById(R.id.nameInputLayout);
        addressInputLayout = (TextInputLayout) findViewById(R.id.addressInputLayout);
        checkInDateInputLayout = (TextInputLayout) findViewById(R.id.checkInDateInputLayout);
        checkInTimeInputLayout = (TextInputLayout) findViewById(R.id.checkInTimeInputLayout);
        checkOutDateInputLayout = (TextInputLayout) findViewById(R.id.checkOutDateInputLayout);
        checkOutTimeInputLayout = (TextInputLayout) findViewById(R.id.checkOutTimeInputLayout);

        addAccommodationButton = (Button) findViewById(R.id.addAccommodationButton);
        nameTextBox = (EditText) findViewById(R.id.nameTextBox);
        addressTextBox = (EditText) findViewById(R.id.addressTextBox);
        checkInDateTextBox = (EditText) findViewById(R.id.checkInDateTextBox);
        checkOutDateTextBox = (EditText) findViewById(R.id.checkOutDateTextBox);
        checkInTimeTextBox = (EditText) findViewById(R.id.checkInTineTextBox);
        checkOutTimeTextBox = (EditText) findViewById(R.id.checkOutTimeTextBox);
        notesTextBox = (EditText) findViewById(R.id.noteTextBox);

        checkInDateTextBox.setInputType(InputType.TYPE_NULL);
        checkOutDateTextBox.setInputType(InputType.TYPE_NULL);
        checkInTimeTextBox.setInputType(InputType.TYPE_NULL);
        checkOutTimeTextBox.setInputType(InputType.TYPE_NULL);

        Journey journey = journeyTable.getJourneyById(journeyId);

        checkInDateTextBox.setText(journey.getStartDate());
        checkOutDateTextBox.setText(journey.getEndDate());

        checkInTimeTextBox.setText("14:00");
        checkOutTimeTextBox.setText("10:00");

        checkInDateTextBox.setOnClickListener(new OnCheckInDateClicked());
        checkOutDateTextBox.setOnClickListener(new OnCheckOutDateClicked());
        checkInTimeTextBox.setOnClickListener(new OnCheckInOutTimeClicked());
        checkOutTimeTextBox.setOnClickListener(new OnCheckInOutTimeClicked());
        addAccommodationButton.setOnClickListener(new OnAddAccommodationButtonClicked());

    }

    @Override
    public void onBackPressed()
    {
        Intent intent = new Intent(this, AccommodationActivity.class);
        intent.putExtra("JOURNEY_ID", journeyId);
        finish();
        startActivity(intent);
    }

    private void getJourneyIdFromExtras()
    {
        Bundle extras = getIntent().getExtras();
        if (extras != null)
        {
            journeyId = extras.getInt("JOURNEY_ID");
        }
    }

    private boolean validateRequiredFields()
    {
        boolean isValid = true;

        if (nameTextBox.getText().toString().trim().equals(""))
        {
            nameInputLayout.setError("Name is required");
            isValid = false;
        }

        if (addressTextBox.getText().toString().trim().equals(""))
        {
            addressInputLayout.setError("Address is required");
            isValid = false;
        }

        if (checkInDateTextBox.getText().toString().trim().equals(""))
        {
            checkInDateInputLayout.setError("Check In Date is required");
            isValid = false;
        }

        if (checkOutDateTextBox.getText().toString().trim().equals(""))
        {
            checkOutDateInputLayout.setError("Check Out Date is required");
            isValid = false;
        }

        if (checkInTimeTextBox.getText().toString().trim().equals(""))
        {
            checkInTimeInputLayout.setError("Check In Time is required");
            isValid = false;
        }

        if (checkOutTimeTextBox.getText().toString().trim().equals(""))
        {
            checkOutTimeInputLayout.setError("Check Out Time is required");
            isValid = false;
        }

        return isValid;
    }

    private class OnAddAccommodationButtonClicked implements View.OnClickListener
    {

        @Override
        public void onClick(View v) {

            if (validateRequiredFields())
            {
                String name = nameTextBox.getText().toString();
                String address = addressTextBox.getText().toString();
                String checkInDate = checkInDateTextBox.getText().toString();
                String checkOutDate = checkOutDateTextBox.getText().toString();
                String checkInTime = checkInTimeTextBox.getText().toString();
                String checkOutTime = checkOutTimeTextBox.getText().toString();
                String notes = notesTextBox.getText().toString();

                accommodationTable.insert(name, address, checkInDate, checkOutDate,
                        checkInTime, checkOutTime, notes, journeyId);

                Intent intent = new Intent(NewAccommodationActivity.this, AccommodationActivity.class);
                intent.putExtra("JOURNEY_ID", journeyId);
                finish();
                startActivity(intent);
            }
        }
    }

    private class OnCheckInDateClicked implements View.OnClickListener
    {

        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        public void onClick(View view) {
            String selectedDate = checkInDateTextBox.getText().toString();
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

            try {
                calendar.setTime(sdf.parse(selectedDate));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            year = calendar.get(Calendar.YEAR);
            month = calendar.get(Calendar.MONTH);
            date = calendar.get(Calendar.DAY_OF_MONTH);

            datePickerDialog = new DatePickerDialog(NewAccommodationActivity.this, new DatePickerDialog.OnDateSetListener()
            {

                @Override
                public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                    checkInDateTextBox.setText(dayOfMonth + "/"
                            + (month + 1) + "/" + year);

                    String selectedDate = checkInDateTextBox.getText().toString();
                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

                    try {
                        calendar.setTime(sdf.parse(selectedDate));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    calendar.add(Calendar.DAY_OF_MONTH, 1);

                    year = calendar.get(Calendar.YEAR);
                    month = calendar.get(Calendar.MONTH);
                    dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);

                    checkOutDateTextBox.setText(dayOfMonth + "/"
                            + (month + 1) + "/" + year);

                }
            }, year, month, date);
            datePickerDialog.show();
        }
    }

    private class OnCheckOutDateClicked implements View.OnClickListener
    {

        @Override
        public void onClick(View v) {
            String selectedDate = checkOutDateTextBox.getText().toString();
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

            try {
                calendar.setTime(sdf.parse(selectedDate));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            year = calendar.get(Calendar.YEAR);
            month = calendar.get(Calendar.MONTH);
            date = calendar.get(Calendar.DAY_OF_MONTH);

            datePickerDialog = new DatePickerDialog(NewAccommodationActivity.this, new DatePickerDialog.OnDateSetListener()
            {

                @Override
                public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                    checkOutDateTextBox.setText(dayOfMonth + "/"
                            + (month + 1) + "/" + year);

                }
            }, year, month, date);
            datePickerDialog.show();
        }
    }

    private class OnCheckInOutTimeClicked implements View.OnClickListener
    {

        @Override
        public void onClick(View v) {
            Log.d(TAG, "OnCheckInOutTimeClicked");

            int hour = 0;
            int minute = 0;
            if (v == checkInTimeTextBox)
            {
                hour = 14;
                TimePickerDialog timePickerDialog = new TimePickerDialog(NewAccommodationActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        String minuteString = "";
                        if (minute == 0)
                        {
                            minuteString = "00";
                        }
                        else
                        {
                            minuteString = String.valueOf(minute);
                        }
                        checkInTimeTextBox.setText(hourOfDay + ":" + minuteString);
                    }
                }, hour, minute, true);
                timePickerDialog.show();
            }
            else if (v == checkOutTimeTextBox)
            {
                hour = 10;
                TimePickerDialog timePickerDialog = new TimePickerDialog(NewAccommodationActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        String minuteString = "";
                        if (minute == 0)
                        {
                            minuteString = "00";
                        }
                        else
                        {
                            minuteString = String.valueOf(minute);
                        }
                        checkOutTimeTextBox.setText(hourOfDay + ":" + minuteString);
                    }
                }, hour, minute, true);
                timePickerDialog.show();
            }


        }
    }
}
