package com.example.sandyphan.travelorganiser.Database.Tables;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.example.sandyphan.travelorganiser.Database.DBHelper;
import com.example.sandyphan.travelorganiser.Objects.Accomodation;
import com.example.sandyphan.travelorganiser.Objects.DayPlan;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sandy.phan on 12/2/17.
 */

public class DayPlanTable extends BaseTable {
    public static final String TAG = DayPlanTable.class.getSimpleName();

    private static final String TABLE_NAME = "day_plan";
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";
    private static final String KEY_DATE = "date";
    private static final String KEY_JOURNEY_ID = "journey_id";

    public DayPlanTable(Context context) {
        super();
        this.mContext = context;
        this.dbHelper = DBHelper.getHelper(mContext);
    }

    public static String createTable()
    {
        return "CREATE TABLE " + TABLE_NAME + " (" +
                KEY_ID + PRIMARY_KEY + NOT_NULL + COMMA_SEP +
                KEY_NAME + TEXT_TYPE + NOT_NULL + COMMA_SEP +
                KEY_DATE + TEXT_TYPE + NOT_NULL + COMMA_SEP +
                KEY_JOURNEY_ID + INTEGER_TYPE + NOT_NULL + COMMA_SEP +
                "FOREIGN KEY(" + KEY_JOURNEY_ID + ") REFERENCES journey(id));";
    }

    public List<DayPlan> getAllDayPlansByJourney(int journeyId)
    {
        String selectQuery = "SELECT * FROM " + TABLE_NAME +
                " WHERE " + KEY_JOURNEY_ID + " = " + journeyId +
                " ORDER BY " + "date(" + KEY_DATE + ");";
        List<DayPlan> dayPlans = new ArrayList<>();

        openDatabase();

        Log.e(TAG, selectQuery);

        Cursor cursor = database.rawQuery(selectQuery, null);

        if (cursor != null && cursor.moveToFirst())
        {
            do
            {
                DayPlan dayPlan = new DayPlan();
                dayPlan.setId(cursor.getInt(cursor.getColumnIndex(KEY_ID)));
                dayPlan.setName(cursor.getString(cursor.getColumnIndex(KEY_NAME)));
                dayPlan.setDate(cursor.getString(cursor.getColumnIndex(KEY_DATE)));

                dayPlans.add(dayPlan);
            } while (cursor.moveToNext());
        }

        cursor.close();

        closeDatabase();

        return dayPlans;
    }

    public DayPlan getDayPlanByName(int journeyId, String name)
    {
        String selectQuery = "SELECT * FROM " + TABLE_NAME +
                " WHERE " + KEY_JOURNEY_ID + " = " + journeyId +
                " AND " + KEY_NAME + " = '" + name + "'";

        DayPlan dayPlan = new DayPlan();

        openDatabase();
        Log.e(TAG, selectQuery);

        Cursor cursor = database.rawQuery(selectQuery, null);

        if (cursor != null && cursor.moveToFirst())
        {
            do
            {
                dayPlan = new DayPlan();
                dayPlan.setId(cursor.getInt(cursor.getColumnIndex(KEY_ID)));
                dayPlan.setName(cursor.getString(cursor.getColumnIndex(KEY_NAME)));
                dayPlan.setDate(cursor.getString(cursor.getColumnIndex(KEY_DATE)));
                dayPlan.setJourneyId(cursor.getInt(cursor.getColumnIndex(KEY_JOURNEY_ID)));
            } while (cursor.moveToNext());
        }

        cursor.close();

        closeDatabase();

        return dayPlan;
    }

    public DayPlan getDayPlanById(int journeyId, int dayPlanId)
    {
        String selectQuery = "SELECT * FROM " + TABLE_NAME +
                " WHERE " + KEY_JOURNEY_ID + " = " + journeyId +
                " AND " + KEY_ID + " = " + dayPlanId + "";

        DayPlan dayPlan = new DayPlan();

        openDatabase();
        Log.e(TAG, selectQuery);

        Cursor cursor = database.rawQuery(selectQuery, null);

        if (cursor != null && cursor.moveToFirst())
        {
            do
            {
                dayPlan = new DayPlan();
                dayPlan.setId(cursor.getInt(cursor.getColumnIndex(KEY_ID)));
                dayPlan.setName(cursor.getString(cursor.getColumnIndex(KEY_NAME)));
                dayPlan.setDate(cursor.getString(cursor.getColumnIndex(KEY_DATE)));
                dayPlan.setJourneyId(cursor.getInt(cursor.getColumnIndex(KEY_JOURNEY_ID)));
            } while (cursor.moveToNext());
        }

        cursor.close();

        closeDatabase();

        return dayPlan;
    }

    public int insert(String name, String date, int journeyId)
    {
        int dayPlanId;
        openDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME, name);
        values.put(KEY_DATE, date);
        values.put(KEY_JOURNEY_ID, journeyId);

        dayPlanId = (int) database.insert(TABLE_NAME, null, values);
        closeDatabase();

        return dayPlanId;
    }
}
