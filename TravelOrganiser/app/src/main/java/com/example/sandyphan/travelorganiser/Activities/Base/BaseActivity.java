package com.example.sandyphan.travelorganiser.Activities.Base;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.example.sandyphan.travelorganiser.Activities.MyJourneyActivity;
import com.example.sandyphan.travelorganiser.Activities.MyExpensesActivity;
import com.example.sandyphan.travelorganiser.Database.DBHelper;
import com.example.sandyphan.travelorganiser.Database.DatabaseManager;
import com.example.sandyphan.travelorganiser.R;

public abstract class BaseActivity extends AppCompatActivity {

//    private BottomNavigationView bottomNavigationView;
    protected Toolbar toolbar;
    private TextView toolbarTitle;

    protected void onCreateBaseActivity() {
        DBHelper dbHelper = new DBHelper(this);

        DatabaseManager.initializeInstance(dbHelper);

//        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbarTitle = (TextView) findViewById(R.id.toolbarTitle);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitle("");
        toolbar.setSubtitle("");

//        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
//            @Override
//            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
//                switch (item.getItemId())
//                {
//                    case R.id.action_my_journey:
//                        openMyJourneyDetailsActivity(bottomNavigationView);
//                        break;
//                    case R.id.action_my_expense:
//                        openMyExpenseActivity(bottomNavigationView);
//                        break;
//                    case R.id.action_settings:
//                        break;
//                }
//                return false;
//            }
//        });
    }

    protected void setToolbarTitle(String name)
    {
        toolbarTitle.setText(name);
    }

//    public void openMyExpenseActivity(View view)
//    {
//        Intent intent = new Intent(this, MyExpensesActivity.class);
//        startActivity(intent);
//    }
//
//    public void openMyJourneyDetailsActivity(View view)
//    {
//        Intent intent = new Intent(this, MyJourneyActivity.class);
//        startActivity(intent);
//    }
}

