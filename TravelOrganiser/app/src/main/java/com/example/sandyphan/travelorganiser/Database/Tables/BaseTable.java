package com.example.sandyphan.travelorganiser.Database.Tables;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import android.util.Log;

import com.example.sandyphan.travelorganiser.Database.DBHelper;
import com.example.sandyphan.travelorganiser.Database.DatabaseManager;

/**
 * Created by Sandy.Phan on 09/12/2016.
 */

public class BaseTable implements BaseColumns {
    protected static final String PRIMARY_KEY = " INTEGER PRIMARY KEY";
    protected static final String TEXT_TYPE = " TEXT";
    protected static final String REAL_TYPE = " REAL";
    protected static final String INTEGER_TYPE = " INTEGER";
    protected static final String NOT_NULL = " NOT NULL";
    protected static final String DATETIME_TYPE = " DATETIME";
    protected static final String COMMA_SEP = ",";
    protected DBHelper dbHelper;
    protected Context mContext;
    protected  SQLiteDatabase database;

    public void openDatabase() throws SQLException
    {
        if(dbHelper == null)
            dbHelper = DBHelper.getHelper(mContext);
        database = dbHelper.getWritableDatabase();
    }

    public void closeDatabase()
    {
        if (database == null)
        {
            return;
        }

        database.close();
    }


    public static void deleteTable(String tableName)
    {
        SQLiteDatabase db = DatabaseManager.getInstance().openDatabase();
        db.delete(tableName, null, null);
        DatabaseManager.getInstance().closeDatabase();
    }

    protected boolean checkTableIsEmpty(String tableName)
    {
        int count = getCount(tableName);

        return count == 0;
    }

    protected int getCount(String tableName)
    {
        String selectQuery = "SELECT * FROM " + tableName;

        openDatabase();

        Cursor cursor = database.rawQuery(selectQuery, null);

        return cursor.getCount();

    }
}