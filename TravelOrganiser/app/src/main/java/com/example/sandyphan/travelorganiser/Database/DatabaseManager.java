package com.example.sandyphan.travelorganiser.Database;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.ContactsContract;

/**
 * Created by Sandy.Phan on 09/12/2016.
 */

public class DatabaseManager {

    private Integer mOpenCounter = 0;

    private static DatabaseManager instance;
    private static SQLiteOpenHelper mDatabaseHelper;
    private SQLiteDatabase mDatabase;

    public static synchronized  DatabaseManager initializeInstance(SQLiteOpenHelper helper)
    {
        if (instance == null)
        {
            instance = new DatabaseManager();
            mDatabaseHelper = helper;
        }

        return instance;
    }

    public static synchronized DatabaseManager getInstance()
    {
        if (instance == null)
        {
            throw new IllegalStateException(DatabaseManager.class.getSimpleName() +
                " is not initialized, call initializeInstance(..) method first.");
        }

        return instance;
    }

    public synchronized SQLiteDatabase openDatabase()
    {
//        mOpenCounter += 1;
//        if (mOpenCounter == 1)
//        {
            mDatabase = mDatabaseHelper.getWritableDatabase();
//        }

        return mDatabase;
    }

    public synchronized  void closeDatabase()
    {
        mOpenCounter -= 1;
        if (mOpenCounter == 0)
        {
            mDatabase.close();
        }
    }
}
