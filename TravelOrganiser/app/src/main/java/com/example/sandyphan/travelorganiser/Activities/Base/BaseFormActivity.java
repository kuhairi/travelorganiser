package com.example.sandyphan.travelorganiser.Activities.Base;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.sandyphan.travelorganiser.Activities.AccommodationActivity;
import com.example.sandyphan.travelorganiser.Activities.MyExpensesActivity;
import com.example.sandyphan.travelorganiser.Activities.MyJourneyActivity;
import com.example.sandyphan.travelorganiser.Activities.NewJourneyActivity;
import com.example.sandyphan.travelorganiser.R;

public abstract class BaseFormActivity extends BaseActivity {

    private final static String TAG = BaseFormActivity.class.getSimpleName();

    protected int journeyId;

//    protected FloatingActionButton cancelButton;

    protected MenuItem cancelButton;
    protected MenuItem deleteButton;

    private boolean isEdit;


    protected void onCreateBaseFormActivity(boolean isEdit) {

        onCreateBaseActivity();

        isEdit = isEdit;

        MenuInflater inflater = new MenuInflater(this);
        inflater.inflate(R.menu.form_toolbar_menu, toolbar.getMenu());

        cancelButton = toolbar.getMenu().findItem(R.id.cancel_button);
        deleteButton = toolbar.getMenu().findItem(R.id.delete_button);

        if (isEdit)
        {
            Log.d(TAG, "setting delete button to visible");
            deleteButton.setVisible(true);
        }

//        cancelButton = (FloatingActionButton) findViewById(R.id.cancel_button);
//
//        LinearLayout toolbarRelativeLayout = (LinearLayout) findViewById(R.id.toolbar_linear_layout);
//
//        cancelButton = new TextView(this);
//        cancelButton.setId(R.id.cancel_button);
//        cancelButton.setBackgroundResource(R.color.colorPrimary);
//        cancelButton.setText(R.string.cancel);
//        cancelButton.setTextColor(Color.WHITE);
//        cancelButton.setTextSize(14);
//
//        cancelButton.setOnClickListener(new OnCancelButtonClicked());
//        toolbarRelativeLayout.addView(cancelButton);
//
//        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
//        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) cancelButton.getLayoutParams();
//        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_END);
//        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
//
//        cancelButton.setLayoutParams(layoutParams);
//
//        addDeleteButton();
    }

//    protected void setIsEdit(boolean value)
//    {
//        isEdit = value;
//    }

//    @Override
//    public boolean onPrepareOptionsMenu(Menu menu)
//    {
//        Log.d(TAG, "in onPrepareOptionsMenu");
//
//        getMenuInflater().inflate(R.menu.form_toolbar_menu, menu);
//        cancelButton = menu.findItem(R.id.cancel_button);
//        deleteButton = menu.findItem(R.id.delete_button);
//
//        if (isEdit == true)
//        {
//            deleteButton.setVisible(true);
//        }
//
//        return super.onPrepareOptionsMenu(menu);
//    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        Log.d(TAG, "in onCreateOptionsMenu");
        getMenuInflater().inflate(R.menu.form_toolbar_menu, menu);
        cancelButton = menu.findItem(R.id.cancel_button);
        deleteButton = menu.findItem(R.id.delete_button);

        Log.d(TAG, "isEdit: " + isEdit);
        if (isEdit)
        {
            deleteButton.setVisible(true);
        }

        return super.onCreateOptionsMenu(menu);
    }

//    protected void setDeleteButtonVisible()
//    {
//        deleteButton.setVisible(true);
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch(item.getItemId())
        {
            case R.id.cancel_button:
                showCancelConfirmationDialog(this);
                return true;
            case R.id.delete_button:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    protected void addDeleteButton()
    {

//        deleteButton = new TextView(this);
//        deleteButton.setBackgroundResource(R.color.colorPrimary);
//        deleteButton.setText(R.string.delete);
//        deleteButton.setTextColor(Color.WHITE);
//        deleteButton.setTextSize(14);
//
//        LinearLayout toolbarRelativeLayout = (LinearLayout) findViewById(R.id.toolbar_linear_layout);
//        toolbarRelativeLayout.addView(deleteButton, 0);
//
//        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) deleteButton.getLayoutParams();
//
//        deleteButton.setLayoutParams(layoutParams);
    }

    protected void showCancelConfirmationDialog(final Context context)
    {
        final AlertDialog.Builder builder = new AlertDialog.Builder(BaseFormActivity.this);
        builder.setTitle(R.string.cancel_confirmation_dialog_title);

        String positiveText = getString(R.string.continue_button);
        String negativeText = getString(R.string.cancel_button);

        builder.setCancelable(true);

        builder.setMessage(getString(R.string.cancel_confirmation_dialog_content));

        builder.setPositiveButton(positiveText, new DialogInterface.OnClickListener()
        {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = null;
                if (context.getClass().getSimpleName().contains("Journey"))
                {
                    intent = new Intent(BaseFormActivity.this, MyJourneyActivity.class);
                } else if (context.getClass().getSimpleName().contains("Expense")) {
                    intent = new Intent(BaseFormActivity.this, MyExpensesActivity.class);
                } else if (context.getClass().getSimpleName().contains("Accommodation"))
                {
                    intent = new Intent(BaseFormActivity.this, AccommodationActivity.class);
                }

                intent.putExtra("JOURNEY_ID", journeyId);
                startActivity(intent);
            }
        });

        builder.setNegativeButton(negativeText, new DialogInterface.OnClickListener()
        {

            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void onBackPressed()
    {
        showCancelConfirmationDialog(this);
    }

    protected class OnCancelButtonClicked implements View.OnClickListener
    {

        @Override
        public void onClick(View v) {
            showCancelConfirmationDialog(v.getContext());
        }
    }
}
