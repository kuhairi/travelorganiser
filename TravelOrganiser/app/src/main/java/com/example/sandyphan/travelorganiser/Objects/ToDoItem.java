package com.example.sandyphan.travelorganiser.Objects;

/**
 * Created by Sandy.Phan on 02/03/2017.
 */

public class ToDoItem extends TravelObject {
    private final static String TAG = ToDoItem.class.getSimpleName();

    private int _journeyId;

    private int isDone;

    public ToDoItem() {super();}

    public int getJourneyId() {
        return _journeyId;
    }

    public void setJourneyId(int _journeyId) {
        this._journeyId = _journeyId;
    }


    public int getIsDone() {
        return isDone;
    }

    public void setIsDone(int isDone) {
        this.isDone = isDone;
    }
}
