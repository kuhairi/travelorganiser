package com.example.sandyphan.travelorganiser.Activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.example.sandyphan.travelorganiser.Activities.Base.BaseDetailsActivity;
import com.example.sandyphan.travelorganiser.Adapters.DestinationsAdapter;
import com.example.sandyphan.travelorganiser.Database.Tables.DayPlanTable;
import com.example.sandyphan.travelorganiser.Database.Tables.DestinationTable;
import com.example.sandyphan.travelorganiser.Objects.Contants;
import com.example.sandyphan.travelorganiser.Objects.DayPlan;
import com.example.sandyphan.travelorganiser.Objects.Destination;
import com.example.sandyphan.travelorganiser.R;

import java.util.List;

public class DestinationsActivity extends BaseDetailsActivity {

    private final static String TAG = DestinationsActivity.class.getSimpleName();

    private int dayPlanId;
    private String previousActivity;

    private DayPlan dayPlan;

    private DestinationTable destinationTable;
    private DayPlanTable dayPlanTable;

    private RecyclerView recyclerView;
    private FloatingActionButton fab;

    private DestinationsAdapter destinationsAdapter;
    private List<Destination> destinationList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_destinations);
        onCreateBaseDetailsActivity();
        getJourneyIdFromExtras();
        getDayPlanIdFromExtras();
        getPreviousActivityFromExtras();

        dayPlanTable = new DayPlanTable(this);

        if (previousActivity.equals(JourneyPlanActivity.class.getSimpleName()))
        {
            dayPlan = dayPlanTable.getDayPlanById(journeyId, dayPlanId);
            setToolbarTitle(dayPlan.getName());
        }
        else
        {
            setToolbarTitle(Contants.DESTINATIONS_TOOLBAR_TITLE);
        }

        destinationTable = new DestinationTable(this);
        destinationList = destinationTable.getAllDestinations(journeyId, dayPlanId);
        Log.d(TAG, "destinationList.size(): " + destinationList.size());

        recyclerView = (RecyclerView) findViewById(R.id.destination_recyler_view);
        fab = (FloatingActionButton) findViewById(R.id.add_destination_button);

        destinationsAdapter = new DestinationsAdapter(this, destinationList);

        fab.setOnClickListener(new OnAddDestinationButtonClicked());

        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this, 1);
        recyclerView.setLayoutManager(layoutManager);

        recyclerView.setAdapter(destinationsAdapter);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goBack();
            }
        });
    }

    @Override
    public void onBackPressed()
    {
        goBack();
    }

    public void goBack()
    {
        Intent intent;
        if (previousActivity.equals(JourneyPlanActivity.class.getSimpleName()))
        {
            intent = new Intent(this, JourneyPlanActivity.class);
            intent.putExtra("DAY_PLAN_ID", dayPlanId);
        }
        else
        {
            intent = new Intent(this, JourneyDetailsActivity.class);
        }
        intent.putExtra("JOURNEY_ID", journeyId);
        finish();
        startActivity(intent);
    }

    private class OnAddDestinationButtonClicked implements View.OnClickListener
    {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(v.getContext(), NewDestinationActivity.class);
            intent.putExtra("JOURNEY_ID", journeyId);
            intent.putExtra("DAY_PLAN_ID", dayPlanId);
            intent.putExtra("CAME_FROM_ACTIVITY", previousActivity);
            startActivity(intent);
        }
    }

    private void getJourneyIdFromExtras()
    {
        Bundle extras = getIntent().getExtras();
        if (extras != null && getIntent().hasExtra("JOURNEY_ID"))
        {
            journeyId = extras.getInt("JOURNEY_ID");
        }
    }

    private void getDayPlanIdFromExtras()
    {
        Bundle extras = getIntent().getExtras();
        if (extras != null && getIntent().hasExtra("DAY_PLAN_ID"))
        {
            dayPlanId = extras.getInt("DAY_PLAN_ID");
        }
        else
        {
            dayPlanId = -1;
        }
    }

    private void getPreviousActivityFromExtras()
    {
        Bundle extras = getIntent().getExtras();
        if (extras != null && getIntent().hasExtra("CAME_FROM_ACTIVITY"))
        {
            previousActivity = extras.getString("CAME_FROM_ACTIVITY");
        }
    }
}
