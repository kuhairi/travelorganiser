package com.example.sandyphan.travelorganiser.Objects;

/**
 * Created by sandy.phan on 5/1/17.
 */

public abstract class TravelObject {
    public static final String TAG = TravelObject.class.getSimpleName();

    protected int _id;
    protected String _name;

    public TravelObject() {}

    public String getName() {
        return _name;
    }

    public void setName(String _name) {
        this._name = _name;
    }

    public int getId() {
        return _id;
    }

    public void setId(int _id) {
        this._id = _id;
    }

    @Override
    public String toString()
    {
        return getName();
    }
}
