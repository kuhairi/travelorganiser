package com.example.sandyphan.travelorganiser.Objects;

/**
 * Created by sandy.phan on 5/1/17.
 */

public class Contants {

    public final static String MY_JOURNEY_TOOLBAR_TITLE = "My Journey";
    public final static String MY_EXPENSE_TOOLBAR_TITLE = "My Expense";
    public final static String NEW_JOURNEY_TOOLBAR_TITLE = "New Journey";
    public final static String NEW_EXPENSE_TOOLBAR_TITLE = "New Expense";
    public final static String ACCOMMODATION_TOOLBAR_TITLE = "Accommodations";
    public final static String NEW_ACCOMMODATION_TOOLBAR_TITLE = "New Accommodation";
    public final static String JOURNEY_PLAN_TOOLBAR_TITLE = "Journey Plans";
    public final static String DESTINATIONS_TOOLBAR_TITLE = "Destinations";
    public final static String NEW_DESTINATION_TOOLBAR_TITLE = "New Destination";
    public final static String CHECK_LIST_TOOLBAR_TITLE = "Check List";
}
