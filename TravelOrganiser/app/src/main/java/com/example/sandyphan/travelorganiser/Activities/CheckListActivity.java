package com.example.sandyphan.travelorganiser.Activities;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.sandyphan.travelorganiser.Activities.Base.BaseDetailsActivity;
import com.example.sandyphan.travelorganiser.Adapters.ViewPagerAdapter;
import com.example.sandyphan.travelorganiser.Fragments.LuggageCheckListFragment;
import com.example.sandyphan.travelorganiser.Fragments.ToDoListFragment;
import com.example.sandyphan.travelorganiser.Objects.Contants;
import com.example.sandyphan.travelorganiser.R;

public class CheckListActivity extends BaseDetailsActivity {

    private final static String TAG = CheckListActivity.class.getSimpleName();

    private TabLayout tabLayout;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_list);
        onCreateBaseDetailsActivity();
        getJourneyIdFromExtras();

        setToolbarTitle(Contants.CHECK_LIST_TOOLBAR_TITLE);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goBack();
            }
        });
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new ToDoListFragment(), "To Do List");
        adapter.addFragment(new LuggageCheckListFragment(), "Luggage Check List");
        viewPager.setAdapter(adapter);
    }

    private void getJourneyIdFromExtras()
    {
        Bundle extras = getIntent().getExtras();
        if (extras != null)
        {
            journeyId = extras.getInt("JOURNEY_ID");
        }
    }

    private void goBack()
    {
        Intent intent = new Intent(this, JourneyDetailsActivity.class);
        intent.putExtra("JOURNEY_ID", journeyId);
        finish();
        startActivity(intent);
    }
}
