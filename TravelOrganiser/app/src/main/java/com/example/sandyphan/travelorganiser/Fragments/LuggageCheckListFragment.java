package com.example.sandyphan.travelorganiser.Fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ListView;

import com.example.sandyphan.travelorganiser.Adapters.LuggageCheckListAdapter;
import com.example.sandyphan.travelorganiser.Database.Tables.LuggageCheckItemTable;
import com.example.sandyphan.travelorganiser.Objects.LuggageCheckItem;
import com.example.sandyphan.travelorganiser.R;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link LuggageCheckListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LuggageCheckListFragment extends Fragment {

    private final static String TAG = LuggageCheckListFragment.class.getSimpleName();

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String JOURNEY_ID = "JOURNEY_ID";

    // TODO: Rename and change types of parameters
    private int journeyId;

    private LuggageCheckListAdapter luggageListAdapter;
    private LuggageCheckItemTable luggageListTable;

    private LinearLayoutManager linearLayoutManager;
    private FloatingActionButton addLuggageItemButton;

//    private OnFragmentInteractionListener mListener;

    public LuggageCheckListFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param journeyId Parameter 1.
     * @return A new instance of fragment LuggageCheckListFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static LuggageCheckListFragment newInstance(int journeyId) {
        LuggageCheckListFragment fragment = new LuggageCheckListFragment();
        Bundle args = new Bundle();
        args.putString(JOURNEY_ID, String.valueOf(journeyId));
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            journeyId = getArguments().getInt(JOURNEY_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_luggage_check_list, container, false);

        luggageListTable = new LuggageCheckItemTable(getActivity());
        List<LuggageCheckItem> luggageList = luggageListTable.getAllLuggageListItems(journeyId);
        Log.d(TAG, "luggageList.size() = " + luggageList.size());

        luggageListAdapter = new LuggageCheckListAdapter(getActivity(), luggageList);

        linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        ListView luggageListRecyclerView = (ListView) view.findViewById(R.id.luggageListListView);
        luggageListRecyclerView.setAdapter(luggageListAdapter);

        addLuggageItemButton = (FloatingActionButton) view.findViewById(R.id.add_luggage_list_item_button);
        addLuggageItemButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAddLuggageItemDialog();
            }
        });

        return view;
    }

    private void showAddLuggageItemDialog()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.add_check_list_item_dialog_title);

        final EditText input = new EditText(getActivity());
        FrameLayout.LayoutParams params = new  FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.leftMargin = 10;
        params.rightMargin = 10;
        input.setLayoutParams(params);

        builder.setView(input);

        builder.setMessage(getString(R.string.add_check_list_item_dialog_message));
        builder.setCancelable(true);

        builder.setPositiveButton(R.string.add, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int button)
            {
                String itemDescription = input.getText().toString();
                luggageListTable.insert(itemDescription, 0, journeyId);

                luggageListAdapter.swapItems(luggageListTable.getAllLuggageListItems(journeyId));
            }
        });

        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        luggageListAdapter.swapItems(luggageListTable.getAllLuggageListItems(journeyId));
    }

    // TODO: Rename method, updatePackingStatus argument and hook method into UI event
//    public void onButtonPressed(Uri uri) {
//        if (mListener != null) {
//            mListener.onFragmentInteraction(uri);
//        }
//    }
//
//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
//    }
//
//    @Override
//    public void onDetach() {
//        super.onDetach();
//        mListener = null;
//    }
//
//    /**
//     * This interface must be implemented by activities that contain this
//     * fragment to allow an interaction in this fragment to be communicated
//     * to the activity and potentially other fragments contained in that
//     * activity.
//     * <p>
//     * See the Android Training lesson <a href=
//     * "http://developer.android.com/training/basics/fragments/communicating.html"
//     * >Communicating with Other Fragments</a> for more information.
//     */
//    public interface OnFragmentInteractionListener {
//        // TODO: Update argument type and name
//        void onFragmentInteraction(Uri uri);
//    }
}
