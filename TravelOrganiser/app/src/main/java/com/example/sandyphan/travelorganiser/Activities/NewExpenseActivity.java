package com.example.sandyphan.travelorganiser.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.sandyphan.travelorganiser.Activities.Base.BaseFormActivity;
import com.example.sandyphan.travelorganiser.Adapters.SpinAdapter;
import com.example.sandyphan.travelorganiser.Database.Tables.CategoryTable;
import com.example.sandyphan.travelorganiser.Database.Tables.CurrencyTable;
import com.example.sandyphan.travelorganiser.Database.Tables.ExpenseTable;
import com.example.sandyphan.travelorganiser.Database.Tables.JourneyTable;
import com.example.sandyphan.travelorganiser.Objects.Category;
import com.example.sandyphan.travelorganiser.Objects.Contants;
import com.example.sandyphan.travelorganiser.Objects.Currency;
import com.example.sandyphan.travelorganiser.Objects.Journey;
import com.example.sandyphan.travelorganiser.R;

import org.w3c.dom.Text;

import java.util.List;

public class NewExpenseActivity extends BaseFormActivity {
    private static final String TAG = NewExpenseActivity.class.getSimpleName();

    private Spinner categorySpinner;
    private Spinner currencySpinner;
    // private Spinner journeySpinner;
    private Button addExpenseButton;
    EditText descriptionTextBox;
    EditText amountTextBox;

    private TextInputLayout descriptionInputLayout;
    private TextInputLayout amountInputLayout;

    private CategoryTable categoryTable;
    private CurrencyTable currencyTable;
    private ExpenseTable expenseTable;
    private JourneyTable journeyTable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_expense);
        super.onCreateBaseFormActivity(false);
        setToolbarTitle(Contants.NEW_EXPENSE_TOOLBAR_TITLE);

        Log.d(TAG, "in onCreate");

        getJourneyIdFromExtras();

        categoryTable = new CategoryTable(this);
        currencyTable = new CurrencyTable(this);
        expenseTable = new ExpenseTable(this);
        journeyTable = new JourneyTable(this);

        descriptionInputLayout = (TextInputLayout) findViewById(R.id.descriptionInputLayout);
        amountInputLayout = (TextInputLayout) findViewById(R.id.amountInputLayout);

        categorySpinner = (Spinner) findViewById(R.id.categorySpinner);
        currencySpinner = (Spinner) findViewById(R.id.currencySpinner);
//        journeySpinner = (Spinner) findViewById(R.id.journeySpinner);
        addExpenseButton = (Button) findViewById(R.id.addExpenseButton);
        descriptionTextBox = (EditText) findViewById(R.id.descriptionTextBox);
        amountTextBox = (EditText) findViewById(R.id.amountTextBox);

        categorySpinner.setOnItemSelectedListener(new CategoryItemSelected());
        currencySpinner.setOnItemSelectedListener(new CurrencyItemSelected());
        addExpenseButton.setOnClickListener(new AddExpenseButtonClicked());

        loadCategoryData();
        loadCurrencyData();
//        loadAvailableJourneys();

        Log.d(TAG, "journeyId: " + journeyId);

        Journey journey = journeyTable.getJourneyById(journeyId);
        Currency currency = currencyTable.getCurrencyByName(journey.getCurrency());
        currencySpinner.setSelection(currency.getId() - 1);
//        journeySpinner.setSelection(journeyId - 1);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.my_expenses_toolbar, menu);
        return true;
    }

    @Override
    public void onBackPressed()
    {
        Intent intent = new Intent(this, MyExpensesActivity.class);
        intent.putExtra("JOURNEY_ID", journeyId);
        finish();
        startActivity(intent);
    }

    public void loadCategoryData()
    {
        List<Category> categoryList = categoryTable.getAllCategories();


        SpinAdapter dataAdapter = new SpinAdapter(this, categoryList);

        // dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        categorySpinner.setAdapter(dataAdapter);
    }

    public void loadCurrencyData()
    {
        List<Currency> currencyList = currencyTable.getAllCurrencies();

        SpinAdapter dataAdapter = new SpinAdapter(this, currencyList);

        //dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        currencySpinner.setAdapter(dataAdapter);
    }

//    public void loadAvailableJourneys()
//    {
//        List<Journey> journeyList = journeyTable.getAllJourneys();
//        SpinAdapter dataAdapter = new SpinAdapter(this, journeyList);
//
//        journeySpinner.setAdapter(dataAdapter);
//    }

    private void getJourneyIdFromExtras()
    {
        Bundle extras = getIntent().getExtras();
        if (extras != null)
        {
            journeyId = extras.getInt("JOURNEY_ID");
        }
    }

    private boolean validateRequiredFields() {
        boolean isValid = true;

        if (descriptionTextBox.getText().toString().trim().equals(""))
        {
            descriptionInputLayout.setError("Description is required");
            isValid = false;
        }

        if (amountTextBox.getText().toString().trim().equals(""))
        {
            amountInputLayout.setError("Amount is required");
            isValid = false;
        }

        return isValid;
    }

    private class CategoryItemSelected implements AdapterView.OnItemSelectedListener
    {

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            Category category = (Category) parent.getItemAtPosition(position);
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    }

    private class CurrencyItemSelected implements  AdapterView.OnItemSelectedListener
    {

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            Currency currency = (Currency) parent.getItemAtPosition(position);
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    }

    private class JourneyItemSelected implements  AdapterView.OnItemSelectedListener
    {

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            Journey journey = (Journey) parent.getItemAtPosition(position);
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    }

    private class AddExpenseButtonClicked implements View.OnClickListener
    {

        @Override
        public void onClick(View view) {

            if (validateRequiredFields())
            {
                String description = descriptionTextBox.getText().toString();
                int currencyId = ((Currency) currencySpinner.getSelectedItem()).getId();
                int categoryId = ((Category)categorySpinner.getSelectedItem()).getId();
                double amount = 0;

                try
                {
                    amount = Double.parseDouble(amountTextBox.getText().toString());
                }
                catch (NumberFormatException e)
                {
                    Log.e(TAG, "AddExpenseButtonClicked class parse double error");
                    e.printStackTrace();
                }


                int id = expenseTable.insert(description, amount, currencyId, categoryId, journeyId);
                Log.e(TAG, "expense id inserted = " + id);

                Intent intent = new Intent(NewExpenseActivity.this, MyExpensesActivity.class);
                intent.putExtra("JOURNEY_ID", journeyId);
                finish();
                startActivity(intent);
            }

        }
    }
}
