package com.example.sandyphan.travelorganiser.Activities.Base;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toolbar;

import com.example.sandyphan.travelorganiser.Activities.AccommodationActivity;
import com.example.sandyphan.travelorganiser.Activities.JourneyDetailsActivity;
import com.example.sandyphan.travelorganiser.Activities.MyExpensesActivity;
import com.example.sandyphan.travelorganiser.Activities.MyJourneyActivity;
import com.example.sandyphan.travelorganiser.Activities.NewAccommodationActivity;
import com.example.sandyphan.travelorganiser.Activities.NewExpenseActivity;
import com.example.sandyphan.travelorganiser.Activities.NewJourneyActivity;
import com.example.sandyphan.travelorganiser.R;

import java.net.URI;

import static android.R.attr.drawable;
import static android.R.attr.paddingRight;

public class BaseDetailsActivity extends BaseActivity {
    private static final String TAG = BaseDetailsActivity.class.getSimpleName();
    private static final int PICKFILE_RESULT_CODE = 1;

    private BottomNavigationView bottomNavigationView;
    private LinearLayout bulkAddLayout;
    private LinearLayout singleAddLayout;
    private LinearLayout mainAddLayout;
    private FloatingActionButton addFromFileButton;
    private FloatingActionButton addFromFormButton;
    private FloatingActionButton addFabButton;

    protected ImageButton backButton;

    private boolean addFabButtonStatus = false;

    protected int journeyId;

    protected void onCreateBaseDetailsActivity() {
        onCreateBaseActivity();

//        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation_details);
        bulkAddLayout = (LinearLayout) findViewById(R.id.bulk_add_layout);
        singleAddLayout = (LinearLayout) findViewById(R.id.single_add_layout);
        mainAddLayout = (LinearLayout) findViewById(R.id.add_fab_layout);
        addFabButton = (FloatingActionButton) findViewById(R.id.add_fab_button);
        addFromFileButton = (FloatingActionButton) findViewById(R.id.load_from_file_button);
        addFromFormButton = (FloatingActionButton) findViewById(R.id.add_from_form_button);

        backButton = new ImageButton(this);
        backButton.setBackgroundResource(R.color.colorPrimary);
        backButton.setImageResource(R.drawable.arrow_left);
        backButton.setPadding(0, 0, 50, 0);

        toolbar.addView(backButton, 0);
//        toolbar.addView(backButton);

        hideSubFab();

//        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
//            @Override
//            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
//                switch (item.getItemId())
//                {
//                    case R.id.action_my_journey_details:
//                        openMyJourneyDetailsActivity(bottomNavigationView);
//                        break;
//                    case R.id.action_my_expense:
//                        openMyExpenseActivity(bottomNavigationView);
//                        break;
//                    case R.id.action_my_accommodation:
//                        openMyAccommodationActivity(bottomNavigationView);
//                        break;
////                    case R.id.action_my_journey_plan:
////                        break;
//                }
//                return false;
//            }
//        });

        if (addFabButton != null &&
            addFromFormButton != null &&
            addFromFileButton != null)
        {
            addFabButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (addFabButtonStatus == true)
                    {
                        hideSubFab();
                    }
                    else
                    {
                        showSubFac();
                    }
                }
            });

            addFromFormButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = null;
                    Context context = v.getContext();
                    if (context.getClass().getSimpleName().contains("Journey"))
                    {
                        intent = new Intent(context, NewJourneyActivity.class);
                    } else if (context.getClass().getSimpleName().contains("Expense")) {
                        intent = new Intent(context, NewExpenseActivity.class);
                    } else if (context.getClass().getSimpleName().contains("Accommodation"))
                    {
                        intent = new Intent(context, NewAccommodationActivity.class);
                    }

                    intent.putExtra("JOURNEY_ID", journeyId);
                    startActivity(intent);
                }
            });

            addFromFileButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    Uri uri = Uri.parse(Environment.getExternalStorageDirectory().getPath());
                    intent.setDataAndType(uri, "*/pdf");
                    startActivity(intent);
                }
            });
        }

    }

    @Override
    public void onBackPressed()
    {
        Intent intent = new Intent(this, MyJourneyActivity.class);
        startActivity(intent);
    }

    protected void hideSubFab()
    {
        if (bulkAddLayout != null &&
            singleAddLayout != null)
        {
            bulkAddLayout.setVisibility(View.INVISIBLE);
            singleAddLayout.setVisibility(View.INVISIBLE);
            addFabButtonStatus = false;
        }

    }

    protected void showSubFac()
    {
        if (bulkAddLayout != null &&
            singleAddLayout != null)
        {
            bulkAddLayout.setVisibility(View.VISIBLE);
            singleAddLayout.setVisibility(View.VISIBLE);
            addFabButtonStatus = true;
        }

    }
}
