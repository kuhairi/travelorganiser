package com.example.sandyphan.travelorganiser.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.sandyphan.travelorganiser.Activities.Base.BaseDetailsActivity;
import com.example.sandyphan.travelorganiser.Adapters.MenuListAdapter;
import com.example.sandyphan.travelorganiser.Database.Tables.DayPlanTable;
import com.example.sandyphan.travelorganiser.Database.Tables.JourneyTable;
import com.example.sandyphan.travelorganiser.Objects.Contants;
import com.example.sandyphan.travelorganiser.Objects.DayPlan;
import com.example.sandyphan.travelorganiser.Objects.Journey;
import com.example.sandyphan.travelorganiser.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class JourneyPlanActivity extends BaseDetailsActivity {

    private final static String TAG = JourneyPlanActivity.class.getSimpleName();

    private JourneyTable journeyTable;
    private DayPlanTable dayPlanTable;
    private Journey journey;

    private ListView journeyPlanListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_journey_plan);
        onCreateBaseDetailsActivity();
        getJourneyIdFromExtras();

        setToolbarTitle(Contants.JOURNEY_PLAN_TOOLBAR_TITLE);
        journeyTable = new JourneyTable(this);
        dayPlanTable = new DayPlanTable(this);
        journey = journeyTable.getJourneyById(journeyId);

        String[] dayListStrings = createDayListStrings();

        journeyPlanListView = (ListView) findViewById(R.id.journey_plan_days_list);
        journeyPlanListView.setAdapter(new MenuListAdapter(this, dayListStrings));
        journeyPlanListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String item = (String) parent.getItemAtPosition(position);
                Log.d(TAG, item);

                DayPlan dayPlan = dayPlanTable.getDayPlanByName(journeyId, item);
                Log.d(TAG, "dayPlanId: " + dayPlan.getId());

                Intent intent = new Intent(JourneyPlanActivity.this, DestinationsActivity.class);
                intent.putExtra("JOURNEY_ID", journeyId);
                intent.putExtra("DAY_PLAN_ID", dayPlan.getId());
                intent.putExtra("CAME_FROM_ACTIVITY", JourneyPlanActivity.class.getSimpleName());
                startActivity(intent);
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goBack();
            }
        });
    }

    @Override
    public void onBackPressed()
    {
        goBack();
    }

    public void goBack()
    {
        Intent intent = new Intent(this, JourneyDetailsActivity.class);
        intent.putExtra("JOURNEY_ID", journeyId);
        finish();
        startActivity(intent);
    }


    private String[] createDayListStrings()
    {
        List<DayPlan> dayPlans = dayPlanTable.getAllDayPlansByJourney(journeyId);

        if (dayPlans.size() == 0)
        {
            int numberOfDays = calculateTravelPeriod();
            String[] days = new String[numberOfDays];

            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
            try
            {
                Date startDate = format.parse(journey.getStartDate());
                Calendar date = Calendar.getInstance();
                date.setTime(startDate);

                for (int i = 0; i < numberOfDays; ++i)
                {
                    date.add(Calendar.DATE, 1);
                    String myDate = format.format(date.getTime());
                    days[i] = "Day " + (i + 1) + " - " + myDate;

                    dayPlanTable.insert(days[i], myDate, journeyId);
                }
            }
            catch (ParseException e)
            {
                e.printStackTrace();
            }

            return days;
        }
        else
        {
            String[] days = new String[dayPlans.size()];
            for (int i = 0; i < dayPlans.size(); ++i)
            {
                String day = dayPlans.get(i).getName();
                days[i] = day;
            }
            return days;
        }


    }


    private int calculateTravelPeriod()
    {
        try
        {
            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");

            Date startDate = format.parse(journey.getStartDate());
            Date endDate = format.parse(journey.getEndDate());

            long diff = endDate.getTime() - startDate.getTime();

            return (int) (diff / (24 * 60 * 60 * 1000));

        }
        catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }

    private void getJourneyIdFromExtras()
    {
        Bundle extras = getIntent().getExtras();
        if (extras != null)
        {
            journeyId = extras.getInt("JOURNEY_ID");
        }
    }
}
