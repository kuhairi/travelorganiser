package com.example.sandyphan.travelorganiser.Database.Tables;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.example.sandyphan.travelorganiser.Database.DBHelper;
import com.example.sandyphan.travelorganiser.Objects.Expense;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Sandy.Phan on 09/12/2016.
 */

public class ExpenseTable extends BaseTable {
    public static final String TAG = ExpenseTable.class.getSimpleName();

    private static final String TABLE_NAME = "expense";
    private static final String KEY_ID = "id";
    private static final String KEY_DESCRIPTION = "description";
    private static final String KEY_AMOUNT = "amount";
    private static final String KEY_CURRENCY_ID = "currency_id";
    private static final String KEY_CATEGORY_ID = "category_id";
    private static final String KEY_DATETIME = "datetime";
    private static final String KEY_JOURNEY_ID = "journey_id";

    public ExpenseTable(Context context)
    {
        super();
        this.mContext = context;
        this.dbHelper = DBHelper.getHelper(mContext);
    }

    public static String createTable()
    {
        return "CREATE TABLE " + TABLE_NAME + " (" +
                KEY_ID + PRIMARY_KEY + NOT_NULL + COMMA_SEP +
                KEY_DESCRIPTION + TEXT_TYPE + NOT_NULL + COMMA_SEP +
                KEY_AMOUNT + REAL_TYPE + NOT_NULL + COMMA_SEP +
                KEY_CURRENCY_ID + INTEGER_TYPE + NOT_NULL + COMMA_SEP +
                KEY_CATEGORY_ID + INTEGER_TYPE + COMMA_SEP +
                KEY_DATETIME + DATETIME_TYPE + NOT_NULL + COMMA_SEP +
                KEY_JOURNEY_ID + INTEGER_TYPE + NOT_NULL + COMMA_SEP +
                "FOREIGN KEY(" + KEY_CURRENCY_ID + ") REFERENCES currency(id)" + COMMA_SEP +
                "FOREIGN KEY(" + KEY_CATEGORY_ID + ") REFERENCES category(id)" + COMMA_SEP +
                "FOREIGN KEY(" + KEY_JOURNEY_ID + ") REFERENCES journey(id));" ;
    }

    public int insert(String description, double amount, int currency, int category, int journeyId)
    {
        int expenseId;
        openDatabase();

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Calendar cal = Calendar.getInstance();

        ContentValues values = new ContentValues();
//        values.put(KEY_ID, expense.getId());
        values.put(KEY_DESCRIPTION, description);
        values.put(KEY_AMOUNT, amount);
        values.put(KEY_CURRENCY_ID, currency);
        values.put(KEY_CATEGORY_ID, category);
        values.put(KEY_DATETIME, dateFormat.format(cal.getTime()));
        values.put(KEY_JOURNEY_ID, journeyId);

        expenseId = (int) database.insert(TABLE_NAME, null, values);
        closeDatabase();

        return expenseId;
    }

    public List<Expense> getExpensesByJourney(int journeyId)
    {
        List<Expense> expenses = new ArrayList<Expense>();

        openDatabase();

        String selectQuery = "SELECT expense.id, expense.description, expense.amount, expense.datetime, category.name as category, currency.name as currency FROM " + TABLE_NAME +
                " INNER JOIN category ON expense.category_id = category.id" +
                " INNER JOIN currency ON expense.currency_id = currency.id" +
                " WHERE " +TABLE_NAME + "." + KEY_JOURNEY_ID + " = " + journeyId +
                " ORDER BY " + KEY_DATETIME + " DESC";

        Log.d(TAG, selectQuery);

        Cursor cursor = database.rawQuery(selectQuery, null);

        if (cursor != null && cursor.moveToFirst())
        {
            do
            {
                Expense expense = new Expense();
                expense.setId(cursor.getInt(cursor.getColumnIndex(KEY_ID)));
                expense.setAmount(cursor.getDouble(cursor.getColumnIndex(KEY_AMOUNT)));
                expense.setName(cursor.getString(cursor.getColumnIndex(KEY_DESCRIPTION)));
                expense.setCategory(cursor.getString(cursor.getColumnIndex("category")));
                expense.setCurrency(cursor.getString(cursor.getColumnIndex("currency")));
                expense.setDatetime(cursor.getString(cursor.getColumnIndex(KEY_DATETIME)));
                // expense.setJourneyId(cursor.getInt(cursor.getColumnIndex(KEY_JOURNEY_ID)));

                expenses.add(expense);
            } while (cursor.moveToNext());
        }

        cursor.close();
        closeDatabase();

        return expenses;
    }

}
