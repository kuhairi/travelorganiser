package com.example.sandyphan.travelorganiser.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.sandyphan.travelorganiser.Objects.Expense;
import com.example.sandyphan.travelorganiser.R;

import java.util.List;

/**
 * Created by sandy.phan on 1/1/17.
 */

public class ExpensesAdapter extends BaseAdapter {

    private final static String TAG = ExpensesAdapter.class.getSimpleName();

    private Context _context;
    private List<Expense> _expenseList;

    public ExpensesAdapter(Context context, List<Expense> expenses)
    {
        this._context = context;
        this._expenseList = expenses;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        final ExpenseViewHolder holder;
        final View view;

        if (convertView != null)
        {
            view = convertView;
        }
        else
        {
            LayoutInflater inflater = (LayoutInflater) parent.getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            view = inflater.inflate(R.layout.expense_list_item, parent, false);

            Expense expenseItem = _expenseList.get(position);

            holder = new ExpenseViewHolder(view);

            holder.description.setText(expenseItem.getName());
            holder.amount.setText(String.format("%.2f", expenseItem.getAmount()));
            holder.category.setText(expenseItem.getCategory());
            holder.dateTime.setText(expenseItem.getDatetime());

            view.setTag(holder);

        }

        return view;
    }

    @Override
    public int getCount() {
        return _expenseList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class ExpenseViewHolder extends  RecyclerView.ViewHolder
    {
        public TextView description, currency, amount, category, dateTime;

        public ExpenseViewHolder(View itemView) {
            super(itemView);

            description = (TextView) itemView.findViewById(R.id.expense_description_textview);
//            currency = (TextView) itemView.findViewById(R.id.expense_currency_textview);
            amount = (TextView) itemView.findViewById(R.id.expense_amount_textview);
            category = (TextView) itemView.findViewById(R.id.expense_category_textview);
            dateTime = (TextView) itemView.findViewById(R.id.expense_datetime_textview);
        }
    }
}
