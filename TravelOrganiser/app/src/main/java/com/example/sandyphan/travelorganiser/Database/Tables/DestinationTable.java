package com.example.sandyphan.travelorganiser.Database.Tables;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.example.sandyphan.travelorganiser.Database.DBHelper;
import com.example.sandyphan.travelorganiser.Objects.Destination;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sandy.phan on 12/2/17.
 */

public class DestinationTable extends BaseTable {

    private final static String TAG = DestinationTable.class.getSimpleName();

    private static final String TABLE_NAME = "destination";
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";
    private static final String KEY_OPENING_HOURS = "opening_hours";
    private static final String KEY_ADDRESS = "address";
    private static final String KEY_HOW_TO_GET_THERE = "how_to_get_there";
    private static final String KEY_ENTRY_COST = "entry_cost";
    private static final String KEY_URL = "url";
    private static final String KEY_NOTE = "notes";
    private static final String KEY_DAY_PLAN_ID = "day_plan_id";
    private static final String KEY_JOURNEY_ID = "journey_id";

    private Context mContext;
    private DBHelper dbHelper;

    public DestinationTable(Context context) {
        super();
        this.mContext = context;
        this.dbHelper = DBHelper.getHelper(mContext);
    }

    public static String createTable()
    {
        return "CREATE TABLE " + TABLE_NAME + " (" +
                KEY_ID + PRIMARY_KEY + NOT_NULL + COMMA_SEP +
                KEY_NAME + TEXT_TYPE + NOT_NULL + COMMA_SEP +
                KEY_OPENING_HOURS + TEXT_TYPE + NOT_NULL + COMMA_SEP +
                KEY_ADDRESS + TEXT_TYPE + NOT_NULL + COMMA_SEP +
                KEY_HOW_TO_GET_THERE + TEXT_TYPE + COMMA_SEP +
                KEY_ENTRY_COST + REAL_TYPE + NOT_NULL + COMMA_SEP +
                KEY_URL + TEXT_TYPE + COMMA_SEP +
                KEY_NOTE + TEXT_TYPE + COMMA_SEP +
                KEY_JOURNEY_ID + INTEGER_TYPE + NOT_NULL + COMMA_SEP +
                KEY_DAY_PLAN_ID + INTEGER_TYPE + NOT_NULL + COMMA_SEP +
                "FOREIGN KEY(" + KEY_DAY_PLAN_ID + ") REFERENCES day_plan(id));" +
                "FOREIGN KEY(" + KEY_JOURNEY_ID + ") REFERENCES journey(id));";
    }

    public List<Destination> getAllDestinations(int journeyId, int dayPlanId)
    {
        String selectQuery = "";
        if (dayPlanId == -1)
        {
            selectQuery = "SELECT * FROM " + TABLE_NAME +
                    " WHERE " + KEY_JOURNEY_ID + " = " + journeyId;
        }
        else
        {
            selectQuery = "SELECT * FROM " + TABLE_NAME +
                    " WHERE " + KEY_JOURNEY_ID + " = " + journeyId +
                    " AND " + KEY_DAY_PLAN_ID + " = " + dayPlanId;
        }

        List<Destination> destinations = new ArrayList<>();

        openDatabase();

        Log.e(TAG, selectQuery);

        Cursor cursor = database.rawQuery(selectQuery, null);

        if (cursor != null && cursor.moveToFirst())
        {
            do
            {
                Destination destination = new Destination();
                destination.setId(cursor.getInt(cursor.getColumnIndex(KEY_ID)));
                destination.setName(cursor.getString(cursor.getColumnIndex(KEY_NAME)));
                destination.setOpeningHours(cursor.getString(cursor.getColumnIndex(KEY_OPENING_HOURS)));
                destination.setAddress(cursor.getString(cursor.getColumnIndex(KEY_ADDRESS)));
                destination.setEntryCost(cursor.getDouble(cursor.getColumnIndex(KEY_ENTRY_COST)));
                destination.setUrl(cursor.getString(cursor.getColumnIndex(KEY_URL)));
                destination.setHowToGetThere(cursor.getString(cursor.getColumnIndex(KEY_HOW_TO_GET_THERE)));
                destination.setNotes(cursor.getString(cursor.getColumnIndex(KEY_NOTE)));

                destinations.add(destination);
            } while (cursor.moveToNext());
        }

        cursor.close();

        closeDatabase();

        return destinations;
    }

    public int insert(String name, String openingHours, String address, String howToGetThere,
                      double entryCost, String url, String notes, int journeyId, int dayPlanId)
    {
        int destinationId;
        openDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME, name);
        values.put(KEY_ADDRESS, address);
        values.put(KEY_OPENING_HOURS, openingHours);
        values.put(KEY_HOW_TO_GET_THERE, howToGetThere);
        values.put(KEY_ENTRY_COST, entryCost);
        values.put(KEY_URL, url);
        values.put(KEY_NOTE, notes);
        values.put(KEY_DAY_PLAN_ID, dayPlanId);
        values.put(KEY_JOURNEY_ID, journeyId);

        destinationId = (int) database.insert(TABLE_NAME, null, values);
        closeDatabase();

        return destinationId;
    }

}
