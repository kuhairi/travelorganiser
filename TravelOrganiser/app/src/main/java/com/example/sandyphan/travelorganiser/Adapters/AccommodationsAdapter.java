package com.example.sandyphan.travelorganiser.Adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.sandyphan.travelorganiser.Activities.AccommodationActivity;
import com.example.sandyphan.travelorganiser.Objects.Accomodation;
import com.example.sandyphan.travelorganiser.R;

import java.util.List;

/**
 * Created by sandy.phan on 31/12/16.
 */

public class AccommodationsAdapter extends RecyclerView.Adapter<AccommodationsAdapter.MyViewHolder> {

    private Context _context;
    private List<Accomodation> _accomodationList;

    public AccommodationsAdapter(Context context, List<Accomodation> accomodations)
    {
        this._context = context;
        this._accomodationList = accomodations;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.accommodation_card, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Accomodation accomodation = _accomodationList.get(position);
        holder.accomodationName.setText(accomodation.getName());
        holder.address.setText(accomodation.getAddress());
        holder.checkInDate.setText(accomodation.getCheckInDate());
        holder.checkOutDate.setText(accomodation.getCheckOutDate());
    }

    @Override
    public int getItemCount() {
        return _accomodationList.size();
    }

    public class MyViewHolder extends  RecyclerView.ViewHolder
    {
        private final String TAG = MyViewHolder.class.getSimpleName();

        public TextView accomodationName, address, checkInDate, checkOutDate;
        public Button getDirectionButton;
        public Accomodation accomodation;

        public MyViewHolder(View itemView) {
            super(itemView);

            accomodationName = (TextView) itemView.findViewById(R.id.accommodation_name);
            address = (TextView) itemView.findViewById(R.id.accommodation_address);
            checkInDate = (TextView) itemView.findViewById(R.id.accommodation_checkin_date);
            checkOutDate = (TextView) itemView.findViewById(R.id.accommodation_checkout_date);
            getDirectionButton = (Button) itemView.findViewById(R.id.getDirectionButton);

            itemView.setOnClickListener(new View.OnClickListener()
            {

                @Override
                public void onClick(View v) {
//                    Intent intent = new Intent(v.getContext(), AccommodationActivity.class);
//                    Log.d(TAG, "accomodation.getId() = " + accomodation.getId());
//                    intent.putExtra("ACCOMODATION_ID", accomodation.getId());
//                    _context.startActivity(intent);
                }
            });

            getDirectionButton.setOnClickListener(new View.OnClickListener()
            {

                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("google.navigation:q=" + address.getText().toString()));
                    Log.d(TAG, "google.navigation:q=" + address.getText().toString());
                    _context.startActivity(intent);
                }
            });
        }
    }
}
