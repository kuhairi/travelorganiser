package com.example.sandyphan.travelorganiser.Adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.sandyphan.travelorganiser.Database.Tables.LuggageCheckItemTable;
import com.example.sandyphan.travelorganiser.Objects.LuggageCheckItem;
import com.example.sandyphan.travelorganiser.R;

import java.util.List;

/**
 * Created by sandy.phan on 31/12/16.
 */

public class LuggageCheckListAdapter extends BaseAdapter {

    private final static String TAG = LuggageCheckListAdapter.class.getSimpleName();

    private Context _context;
    private List<LuggageCheckItem> _luggageCheckItems;
    private LuggageCheckItem luggageCheckItem;
    LuggageCheckItemTable luggageCheckItemTable;

    public LuggageCheckListAdapter(Context context, List<LuggageCheckItem> luggageList)
    {
        this._context = context;
        this._luggageCheckItems = luggageList;
        luggageCheckItemTable = new LuggageCheckItemTable(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final MyViewHolder holder;
        final View view;

        if (convertView != null)
        {
            view = convertView;
        }
        else
        {
            LayoutInflater inflater = (LayoutInflater) parent.getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            view = inflater.inflate(R.layout.check_list_item, parent, false);

            holder = new MyViewHolder(view);

            luggageCheckItem = _luggageCheckItems.get(position);

            holder.listItemName.setText(luggageCheckItem.getName());
            holder.luggageCheckItem = luggageCheckItem;
            holder.checkBoxButton.setTag(position);
            holder.deleteButton.setTag(position);
            holder.editButton.setTag(position);

            Log.d(TAG, "listItemName = " + holder.listItemName.getText().toString());

            // change the icon for Windows and iPhone
            if (luggageCheckItem.getIsPacked() == 1)
            {
                holder.checkBoxButton.setImageResource(R.drawable.checkbox_marked);
                holder.listItemName.setPaintFlags(holder.listItemName.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                holder.listItemName.setTypeface(null, Typeface.ITALIC);
            }
            else
            {
                holder.checkBoxButton.setImageResource(R.drawable.checkbox_blank_outline);
            }

            holder.checkBoxButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = (Integer) v.getTag();
                    onClickUpdateCheckBox(holder, position);
                }
            });

            holder.editButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = (Integer) v.getTag();
                    onClickEditItem(holder, view, position);
                }
            });

            holder.deleteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = (Integer) v.getTag();
                    onClickDeleteItem(holder, view, position);
                }
            });

            view.setTag(holder);
        }


        return view;
    }

    public List<LuggageCheckItem> getLuggageListItems()
    {
        return _luggageCheckItems;
    }

    @Override
    public int getCount() {
        return _luggageCheckItems.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void swapItems(List<LuggageCheckItem> items)
    {
        this._luggageCheckItems = items;
        notifyDataSetChanged();
    }

    private void onClickUpdateCheckBox(MyViewHolder holder, int position)
    {
        LuggageCheckItem item =  luggageCheckItemTable.getAllLuggageListItems(holder.journeyId).get(position);


        Log.d(TAG, "clicked item.isPacked before updatePackingStatus = " + item.getIsPacked());
        if (item.getIsPacked() == 0)
        {
            Log.d(TAG, "item is not yet packed, updating to packed");
            holder.checkBoxButton.setImageResource(R.drawable.checkbox_marked);
            holder.listItemName.setPaintFlags(holder.listItemName.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            holder.listItemName.setTypeface(null, Typeface.ITALIC);
            luggageCheckItemTable.updatePackingStatus(item, 1);
        }
        else
        {
            Log.d(TAG, "item is already packed, updating to not packed");
            holder.checkBoxButton.setImageResource(R.drawable.checkbox_blank_outline);
            holder.listItemName.setTypeface(null, Typeface.NORMAL);
            holder.listItemName.setPaintFlags(holder.listItemName.getPaintFlags() & (~ Paint.STRIKE_THRU_TEXT_FLAG));
            luggageCheckItemTable.updatePackingStatus(item, 0);
        }

        Log.d(TAG, "clicked item.name = " + item.getName());
        Log.d(TAG, "clicked item.isPacked after updatePackingStatus = " + item.getIsPacked());
        swapItems(luggageCheckItemTable.getAllLuggageListItems(holder.journeyId));
    }

    private void onClickEditItem(final MyViewHolder holder, View view, int position)
    {
        final LuggageCheckItem item =  luggageCheckItemTable.getAllLuggageListItems(holder.journeyId).get(position);

        AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
        builder.setTitle(R.string.edit_check_list_item_dialog_title);

        final EditText input = new EditText(view.getContext());
        FrameLayout.LayoutParams params = new  FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.leftMargin = 10;
        params.rightMargin = 10;
        input.setLayoutParams(params);
        input.setText(holder.listItemName.getText().toString());

        builder.setView(input);

        builder.setMessage(R.string.add_check_list_item_dialog_message);
        builder.setCancelable(true);

        builder.setPositiveButton(R.string.save, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int button)
            {
                String itemDescription = input.getText().toString();
                luggageCheckItemTable.updateItemDescription(item, itemDescription);
                swapItems(luggageCheckItemTable.getAllLuggageListItems(holder.journeyId));
            }
        });

        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void onClickDeleteItem(final MyViewHolder holder, View view, int position)
    {
        final LuggageCheckItem item =  luggageCheckItemTable.getAllLuggageListItems(holder.journeyId).get(position);

        Log.d(TAG, "deleting item id = " + item.getId());

        AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
        builder.setTitle(R.string.delete_check_list_item_dialog_title);

        builder.setCancelable(true);

        builder.setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int button)
            {
                luggageCheckItemTable.deleteItem(item);
                swapItems(luggageCheckItemTable.getAllLuggageListItems(holder.journeyId));
            }
        });

        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }



    public class MyViewHolder extends  RecyclerView.ViewHolder
    {
        private final String TAG = MyViewHolder.class.getSimpleName();

        public TextView listItemName;
        public ImageButton deleteButton, checkBoxButton, editButton;
        public LuggageCheckItem luggageCheckItem;
        public int journeyId;

        public MyViewHolder(final View itemView) {
            super(itemView);

            listItemName = (TextView) itemView.findViewById(R.id.listItemDescription);
            deleteButton = (ImageButton) itemView.findViewById(R.id.itemDeleteButton);
            checkBoxButton = (ImageButton) itemView.findViewById(R.id.itemCheckBox);
            editButton = (ImageButton) itemView.findViewById(R.id.itemEditButton);
        }
    }
}
