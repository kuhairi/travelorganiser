package com.example.sandyphan.travelorganiser.Activities.Base;

import android.content.Intent;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.sandyphan.travelorganiser.Activities.MyJourneyActivity;
import com.example.sandyphan.travelorganiser.R;

public class BaseHomeActivity extends BaseActivity {

    private final static String TAG = BaseHomeActivity.class.getSimpleName();

    private BottomNavigationView bottomNavigationView;
    private Boolean exit = false;

    protected void onCreateBaseHomeActivity() {
        onCreateBaseActivity();

        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation_main);

        Log.d(TAG, Integer.toString(bottomNavigationView.getMaxItemCount()));

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId())
                {
                    case R.id.action_my_journey:
                        openMyJourneyDetailsActivity(bottomNavigationView);
                        break;
                    case R.id.action_settings:
                        break;
                }
                return false;
            }
        });
    }


    @Override
    public void onBackPressed() {
        if (exit) {
            finishAffinity(); // finish activity
        } else {
            Toast.makeText(this, "Press Back again to Exit.",
                    Toast.LENGTH_SHORT).show();
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 3 * 1000);

        }

    }

    public void openMyJourneyDetailsActivity(View view)
    {
        Intent intent = new Intent(this, MyJourneyActivity.class);
        finish();
        startActivity(intent);
    }
}
