package com.example.sandyphan.travelorganiser.Database.Tables;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.example.sandyphan.travelorganiser.Database.DBHelper;
import com.example.sandyphan.travelorganiser.Objects.Accomodation;
import com.example.sandyphan.travelorganiser.Objects.Category;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sandy.phan on 15/1/17.
 */

public class AccommodationTable extends BaseTable {


    public static final String TAG = AccommodationTable.class.getSimpleName();

    private static final String TABLE_NAME = "accommodation";
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";
    private static final String KEY_DATE_CHECK_IN = "date_check_in";
    private static final String KEY_DATE_CHECK_OUT = "date_check_out";
    private static final String KEY_TIME_CHECK_IN = "time_check_in";
    private static final String KEY_TIME_CHECK_OUT = "time_check_out";
    private static final String KEY_NOTES = "notes";
    private static final String KEY_ADDRESS = "address";
    private static final String KEY_JOURNEY_ID = "journey_id";

    public AccommodationTable(Context context) {
        super();
        this.mContext = context;
        this.dbHelper = DBHelper.getHelper(mContext);
    }

    public static String createTable()
    {
        return "CREATE TABLE " + TABLE_NAME + " (" +
                KEY_ID + PRIMARY_KEY + NOT_NULL + COMMA_SEP +
                KEY_NAME + TEXT_TYPE + NOT_NULL + COMMA_SEP +
                KEY_DATE_CHECK_IN + TEXT_TYPE + NOT_NULL + COMMA_SEP +
                KEY_DATE_CHECK_OUT + TEXT_TYPE + NOT_NULL + COMMA_SEP +
                KEY_TIME_CHECK_IN + TEXT_TYPE + NOT_NULL + COMMA_SEP +
                KEY_TIME_CHECK_OUT + TEXT_TYPE + NOT_NULL + COMMA_SEP +
                KEY_ADDRESS + TEXT_TYPE + NOT_NULL + COMMA_SEP +
                KEY_NOTES + TEXT_TYPE + COMMA_SEP +
                KEY_JOURNEY_ID + INTEGER_TYPE + NOT_NULL + COMMA_SEP +
                "FOREIGN KEY(" + KEY_JOURNEY_ID + ") REFERENCES journey(id));";
    }

    public List<Accomodation> getAllAccommodationsByJourney(int journeyId)
    {
        String selectQuery = "SELECT * FROM " + TABLE_NAME +
                             " WHERE " + KEY_JOURNEY_ID + " = " + journeyId +
                             " ORDER BY " + KEY_DATE_CHECK_IN + " ASC ";
        List<Accomodation> accomodations = new ArrayList<>();

        openDatabase();

        Log.e(TAG, selectQuery);

        Cursor cursor = database.rawQuery(selectQuery, null);

        if (cursor != null && cursor.moveToFirst())
        {
            do
            {
                Accomodation accomodation = new Accomodation();
                accomodation.setId(cursor.getInt(cursor.getColumnIndex(KEY_ID)));
                accomodation.setName(cursor.getString(cursor.getColumnIndex(KEY_NAME)));
                accomodation.setAddress(cursor.getString(cursor.getColumnIndex(KEY_ADDRESS)));
                accomodation.setCheckInDate(cursor.getString(cursor.getColumnIndex(KEY_DATE_CHECK_IN)));
                accomodation.setCheckOutDate(cursor.getString(cursor.getColumnIndex(KEY_DATE_CHECK_OUT)));
                accomodation.setCheckInTime(cursor.getString(cursor.getColumnIndex(KEY_TIME_CHECK_IN)));
                accomodation.setCheckOutTime(cursor.getString(cursor.getColumnIndex(KEY_TIME_CHECK_OUT)));
                accomodation.setNotes(cursor.getString(cursor.getColumnIndex(KEY_NOTES)));

                accomodations.add(accomodation);
            } while (cursor.moveToNext());
        }

        cursor.close();

        closeDatabase();

        return accomodations;
    }

    public int insert(String name, String address, String checkInDate, String checkOutDate,
                      String checkInTime, String checkOutTime, String notes, int journeyId)
    {
        int accommodationId;
        openDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME, name);
        values.put(KEY_ADDRESS, address);
        values.put(KEY_DATE_CHECK_IN, checkInDate);
        values.put(KEY_DATE_CHECK_OUT, checkOutDate);
        values.put(KEY_NOTES, notes);
        values.put(KEY_TIME_CHECK_IN, checkInTime);
        values.put(KEY_TIME_CHECK_OUT, checkOutTime);
        values.put(KEY_JOURNEY_ID, journeyId);

        accommodationId = (int) database.insert(TABLE_NAME, null, values);
        closeDatabase();

        return accommodationId;
    }
}
