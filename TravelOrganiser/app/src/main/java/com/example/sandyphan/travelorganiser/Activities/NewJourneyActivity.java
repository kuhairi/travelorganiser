package com.example.sandyphan.travelorganiser.Activities;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TextInputLayout;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.sandyphan.travelorganiser.Activities.Base.BaseFormActivity;
import com.example.sandyphan.travelorganiser.Adapters.SpinAdapter;
import com.example.sandyphan.travelorganiser.Database.Tables.CountryTable;
import com.example.sandyphan.travelorganiser.Database.Tables.CurrencyTable;
import com.example.sandyphan.travelorganiser.Database.Tables.JourneyTable;
import com.example.sandyphan.travelorganiser.Objects.Contants;
import com.example.sandyphan.travelorganiser.Objects.Country;
import com.example.sandyphan.travelorganiser.Objects.Currency;
import com.example.sandyphan.travelorganiser.Objects.Journey;
import com.example.sandyphan.travelorganiser.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

public class NewJourneyActivity extends BaseFormActivity {
    private static final String TAG = NewJourneyActivity.class.getSimpleName();
    private static final int JOURNEY_ID = 1;

    private DatePickerDialog datePickerDialog;
    private Calendar calendar;
    private int year, month, date;

    private String previousActivity;
    private int journeyId;

    private TextInputLayout nameInputLayout;
    private TextInputLayout destinationInputLayout;
    private TextInputLayout numberOfPeopleInputLayout;
    private TextInputLayout startDateInputLayout;
    private TextInputLayout endDateInputLayout;
    private TextInputLayout exchangeRateInputLayout;

    private Spinner currencySpinner;
    private Button positiveButton;
    private Spinner countrySpinner;
    private EditText nameTextBox;
    private EditText destinationTextBox;
    private EditText startDateTextBox;
    private EditText endDateTextBox;
    private EditText numberOfPeopleTextBox;
    private EditText exchangeRateTextBox;

    private CurrencyTable currencyTable;
    private CountryTable countryTable;
    private JourneyTable journeyTable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_journey);

        getJourneyIdFromExtras();
        getPreviousActivity();

        Log.d(TAG, "previousActivity: " + previousActivity);

        if (previousActivity.equals(JourneyDetailsActivity.class.getSimpleName()))
        {
            Log.d(TAG, "came from journey details activity");
            onCreateBaseFormActivity(true);
        }
        else
        {
            onCreateBaseFormActivity(false);
        }

        setToolbarTitle(Contants.NEW_JOURNEY_TOOLBAR_TITLE);

        getJourneyIdFromExtras();
        getPreviousActivity();
        
        currencyTable = new CurrencyTable(this);
        journeyTable = new JourneyTable(this);
        countryTable = new CountryTable(this);

        nameInputLayout = (TextInputLayout) findViewById(R.id.nameInputLayout);
        destinationInputLayout = (TextInputLayout) findViewById(R.id.destinationInputLayout);
        numberOfPeopleInputLayout = (TextInputLayout) findViewById(R.id.numberOfPeopleInputLayout);
        startDateInputLayout = (TextInputLayout) findViewById(R.id.startDateInputLayout);
        endDateInputLayout = (TextInputLayout) findViewById(R.id.endDateInputLayout);
        exchangeRateInputLayout = (TextInputLayout) findViewById(R.id.exchangeRateInputLayout);

        currencySpinner = (Spinner) findViewById(R.id.currencySpinner);
        countrySpinner = (Spinner) findViewById(R.id.countrySpinner);
        positiveButton = (Button) findViewById(R.id.positiveButton);

        nameTextBox = (EditText) findViewById(R.id.nameTextBox);

        destinationTextBox = (EditText) findViewById(R.id.destinationTextBox);
        numberOfPeopleTextBox = (EditText) findViewById(R.id.numberOfPeopleTextBox);
        exchangeRateTextBox = (EditText) findViewById(R.id.exchangeRateTextBox);

        startDateTextBox = (EditText) findViewById(R.id.startDateTextBox);
        startDateTextBox.setInputType(InputType.TYPE_NULL);

        endDateTextBox = (EditText) findViewById(R.id.endDateTextBox);
        endDateTextBox.setInputType(InputType.TYPE_NULL);

        currencySpinner.setOnItemSelectedListener(new CurrencyItemSelected());
        countrySpinner.setOnItemSelectedListener(new CountryItemSelected());

        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        date = calendar.get(Calendar.DAY_OF_MONTH);

        startDateTextBox.setText((date) + "/"
                + (month + 1) + "/" + year);

        endDateTextBox.setText((date) + "/"
                + (month + 1) + "/" + year);

        startDateTextBox.setOnClickListener(new OnStartDateClicked());
        endDateTextBox.setOnClickListener(new OnEndDateClicked());
        positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onAddJourneyButtonClicked();
            }
        });

        loadData();

        Journey journey = journeyTable.getJourneyById(journeyId);

        if (previousActivity != null && previousActivity.equals(JourneyDetailsActivity.class.getSimpleName()))
        {
            Log.d(TAG, "previousActivity: " + previousActivity);
            setFormToEditView(journey);
        }

    }

    public void onAddJourneyButtonClicked()
    {
        if (validateRequiredFields())
        {
            String name = nameTextBox.getText().toString();
            String destination = destinationTextBox.getText().toString();
            String currency = currencySpinner.getSelectedItem().toString();
            String startDate = startDateTextBox.getText().toString();
            String endDate = endDateTextBox.getText().toString();
            double exchangeRate = Double.parseDouble(exchangeRateTextBox.getText().toString());
            int numberOfPeople = Integer.parseInt(numberOfPeopleTextBox.getText().toString());

            journeyTable.insert(name, destination, currency, startDate, endDate, numberOfPeople, exchangeRate);

            Intent intent = new Intent(NewJourneyActivity.this, MyJourneyActivity.class);
            finish();
            startActivity(intent);
        }

    }

    public void loadData()
    {
        List<Currency> currencyList = currencyTable.getAllCurrencies();
        SpinAdapter spinAdapter = new SpinAdapter(this, currencyList);
        currencySpinner.setAdapter(spinAdapter);

        List<Country> countryList = countryTable.getAllCountries();
        spinAdapter = new SpinAdapter(this, countryList);
        countrySpinner.setAdapter(spinAdapter);
    }

    @Override
    public void onBackPressed()
    {
        Intent intent = new Intent(this, MyJourneyActivity.class);
        finish();
        startActivity(intent);
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//
//        getMenuInflater().inflate(R.menu.form_toolbar_menu, menu);
//        cancelButton = menu.findItem(R.id.cancel_button);
//        deleteButton = menu.findItem(R.id.delete_button);
//        deleteButton.setVisible(true);
//        return true;
//    }

    private void setFormToEditView(Journey journey)
    {
        nameTextBox.setText(journey.getName());
        destinationTextBox.setText(journey.getDestination());
        Log.d(TAG, "numberOfPeople: " + journey.getNumberOfPeople());
        numberOfPeopleTextBox.setText(String.valueOf(journey.getNumberOfPeople()));
        startDateTextBox.setText(journey.getStartDate());
        endDateTextBox.setText(journey.getEndDate());
        exchangeRateTextBox.setText(String.format("%.4f", journey.getExchangeRate()));

        Currency currency = currencyTable.getCurrencyByName(journey.getCurrency());
        currencySpinner.setSelection(currency.getId() - 1);
        countrySpinner.setSelection(currency.getId() - 1);

        positiveButton.setText("SAVE");

//        Menu menu = toolbar.getMenu();
//        Log.d(TAG, "size of toolbar menu: " + menu.size());
//        menu.findItem(R.id.delete_button).setVisible(true);

        setToolbarTitle("Edit Journey");

    }

    private void getJourneyIdFromExtras()
    {
        Bundle extras = getIntent().getExtras();
        if (extras != null && getIntent().hasExtra("JOURNEY_ID"))
        {
            journeyId = extras.getInt("JOURNEY_ID");
        }
    }

    private void getPreviousActivity()
    {
        Bundle extras = getIntent().getExtras();
        if (extras != null && getIntent().hasExtra("CAME_FROM_ACTIVITY"))
        {
            previousActivity = extras.getString("CAME_FROM_ACTIVITY");
        }
    }

    private boolean validateRequiredFields()
    {
        boolean isValid = true;

        if (nameTextBox.getText().toString().trim().equals(""))
        {
            nameInputLayout.setError(("Name is required"));
            isValid = false;
        }

        if (destinationTextBox.getText().toString().trim().equals(""))
        {
            destinationInputLayout.setError(("Destinations is required"));
            isValid = false;
        }

        if (numberOfPeopleTextBox.getText().toString().trim().equals(""))
        {
            numberOfPeopleInputLayout.setError(("Number of People is required"));
            isValid = false;
        }

        if (startDateTextBox.getText().toString().trim().equals(""))
        {
            startDateInputLayout.setError(("Start Date is required"));
            isValid = false;
        }

        if (endDateTextBox.getText().toString().trim().equals(""))
        {
            endDateInputLayout.setError(("End Date is required"));
            isValid = false;
        }

        if (exchangeRateTextBox.getText().toString().trim().equals(""))
        {
            exchangeRateInputLayout.setError("Exchange Rate is required");
            isValid = false;
        }

        return isValid;
    }

    private class CurrencyItemSelected implements  AdapterView.OnItemSelectedListener
    {

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            Currency currency = (Currency) parent.getItemAtPosition(position);
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    }

    private class CountryItemSelected implements  AdapterView.OnItemSelectedListener
    {

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            Country country = (Country) parent.getItemAtPosition(position);

            currencySpinner.setSelection(position);
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    }

    private class OnStartDateClicked implements View.OnClickListener
    {

        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        public void onClick(View view) {
            String selectedDate = startDateTextBox.getText().toString();
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

            try {
                calendar.setTime(sdf.parse(selectedDate));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            year = calendar.get(Calendar.YEAR);
            month = calendar.get(Calendar.MONTH);
            date = calendar.get(Calendar.DAY_OF_MONTH);

            datePickerDialog = new DatePickerDialog(NewJourneyActivity.this, new DatePickerDialog.OnDateSetListener()
            {

                @Override
                public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                    startDateTextBox.setText(dayOfMonth + "/"
                            + (month + 1) + "/" + year);

                    endDateTextBox.setText((dayOfMonth) + "/"
                            + (month + 1) + "/" + year);

                }
            }, year, month, date);
            datePickerDialog.show();
        }
    }

    private class OnEndDateClicked implements View.OnClickListener
    {

        @Override
        public void onClick(View v) {
            String selectedDate = endDateTextBox.getText().toString();
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

            try {
                calendar.setTime(sdf.parse(selectedDate));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            year = calendar.get(Calendar.YEAR);
            month = calendar.get(Calendar.MONTH);
            date = calendar.get(Calendar.DAY_OF_MONTH);

            datePickerDialog = new DatePickerDialog(NewJourneyActivity.this, new DatePickerDialog.OnDateSetListener()
            {

                @Override
                public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                    endDateTextBox.setText(dayOfMonth + "/"
                            + (month + 1) + "/" + year);

                }
            }, year, month, date);
            datePickerDialog.show();
        }
    }

}
