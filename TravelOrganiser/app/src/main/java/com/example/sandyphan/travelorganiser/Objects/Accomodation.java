package com.example.sandyphan.travelorganiser.Objects;

/**
 * Created by Sandy.Phan on 09/12/2016.
 */

public class Accomodation extends TravelObject {
    public static final String TAG = Accomodation.class.getSimpleName();

    private String _address;
    private String _contact;
    private String _url;
    private String _checkInDate;
    private String _checkOutDate;
    private String _checkInTime;
    private String _checkOutTime;
    private String _notes;
    private int _journeyId;

    public Accomodation()
    {
        super();
    }

    public String getAddress() {
        return _address;
    }

    public void setAddress(String _address) {
        this._address = _address;
    }

    public String getContact() {
        return _contact;
    }

    public void setContact(String _contact) {
        this._contact = _contact;
    }

    public String getUrl() {
        return _url;
    }

    public void setUrl(String _url) {
        this._url = _url;
    }

    public int getJourneyId() {
        return _journeyId;
    }

    public void setJourneyId(int _journeyId) {
        this._journeyId = _journeyId;
    }

    public String getCheckOutDate() {
        return _checkOutDate;
    }

    public void setCheckOutDate(String _checkOutDate) {
        this._checkOutDate = _checkOutDate;
    }

    public String getCheckInDate() {
        return _checkInDate;
    }

    public void setCheckInDate(String _checkInDate) {
        this._checkInDate = _checkInDate;
    }

    public String getCheckInTime() {
        return _checkInTime;
    }

    public void setCheckInTime(String _checkInTime) {
        this._checkInTime = _checkInTime;
    }

    public String getCheckOutTime() {
        return _checkOutTime;
    }

    public void setCheckOutTime(String _checkOutTime) {
        this._checkOutTime = _checkOutTime;
    }

    public String getNotes() {
        return _notes;
    }

    public void setNotes(String _notes) {
        this._notes = _notes;
    }
}
