package com.example.sandyphan.travelorganiser.Database.Tables;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.example.sandyphan.travelorganiser.Database.DBHelper;
import com.example.sandyphan.travelorganiser.Objects.ToDoItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sandy.Phan on 02/03/2017.
 */

public class ToDoListTable extends BaseTable {
    private final static String TAG = ToDoListTable.class.getSimpleName();

    private static final String TABLE_NAME = "to_do_list";
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";
    private static final String KEY_IS_DONE = "is_done";
    private static final String KEY_JOURNEY_ID = "journey_id";

    public ToDoListTable(Context context) {
        super();
        this.mContext = context;
        this.dbHelper = DBHelper.getHelper(mContext);
    }

    public static String createTable()
    {
        return "CREATE TABLE " + TABLE_NAME + " (" +
                KEY_ID + PRIMARY_KEY + NOT_NULL + COMMA_SEP +
                KEY_NAME + TEXT_TYPE + NOT_NULL + COMMA_SEP +
                KEY_IS_DONE + INTEGER_TYPE + NOT_NULL + COMMA_SEP +
                KEY_JOURNEY_ID + INTEGER_TYPE + NOT_NULL + COMMA_SEP +
                "FOREIGN KEY(" + KEY_JOURNEY_ID + ") REFERENCES journey(id));";
    }

    public List<ToDoItem> getAllToDoItems(int journeyId)
    {
        String selectQuery = "SELECT * FROM " + TABLE_NAME +
                " WHERE " + KEY_JOURNEY_ID + " = " + journeyId;
        List<ToDoItem> toDoItems = new ArrayList<>();

        openDatabase();

        Log.e(TAG, selectQuery);

        Cursor cursor = database.rawQuery(selectQuery, null);

        if (cursor != null && cursor.moveToFirst())
        {
            do
            {
                ToDoItem toDoItem = new ToDoItem();
                toDoItem.setId(cursor.getInt(cursor.getColumnIndex(KEY_ID)));
                toDoItem.setName(cursor.getString(cursor.getColumnIndex(KEY_NAME)));
                toDoItem.setIsDone(cursor.getInt(cursor.getColumnIndex(KEY_IS_DONE)));

                toDoItems.add(toDoItem);
            } while (cursor.moveToNext());
        }

        cursor.close();

        closeDatabase();

        return toDoItems;
    }

    public int insert(String name, int isDone, int journeyId)
    {
        int toDoItemId;
        openDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME, name);
        values.put(KEY_JOURNEY_ID, journeyId);
        values.put(KEY_IS_DONE, isDone);

        toDoItemId = (int) database.insert(TABLE_NAME, null, values);
        closeDatabase();

        return toDoItemId;
    }



    public void updateDoneStatus(ToDoItem toDoItem, int isDOne)
    {
        ContentValues cv = new ContentValues();
        cv.put(KEY_IS_DONE, isDOne);

        openDatabase();

        database.update(TABLE_NAME, cv, "id = " + toDoItem.getId(), null);

        closeDatabase();
    }

    public void updateItemDescription(ToDoItem toDoItem, String description)
    {
        ContentValues cv = new ContentValues();
        cv.put(KEY_NAME, description);

        openDatabase();

        database.update(TABLE_NAME, cv, "id = " + toDoItem.getId(), null);

        closeDatabase();
    }

    public void deleteItem(ToDoItem toDoItem)
    {
        openDatabase();

        String query = "DELETE FROM " + TABLE_NAME + " WHERE " + KEY_ID + " = " + toDoItem.getId();
        database.execSQL(query);

        closeDatabase();

    }
}
