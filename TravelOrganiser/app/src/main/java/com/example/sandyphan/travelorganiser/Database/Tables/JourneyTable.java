package com.example.sandyphan.travelorganiser.Database.Tables;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.example.sandyphan.travelorganiser.Database.DBHelper;
import com.example.sandyphan.travelorganiser.Objects.Expense;
import com.example.sandyphan.travelorganiser.Objects.Journey;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sandy.Phan on 09/12/2016.
 */

public class JourneyTable extends BaseTable {
    private static final String TAG = JourneyTable.class.getSimpleName();

    private static final String TABLE_NAME = "journey";
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";
    private static final String KEY_DESTINATION = "destination";
    private static final String KEY_CURRENCY = "currency";
    private static final String KEY_EXCHANGE_RATE = "exchange_rate";
    private static final String KEY_START_DATE = "start_date";
    private static final String KEY_END_DATE = "end_date";
    private static final String KEY_NUM_OF_PEOPLE = "number_of_people";
    private static final String KEY_NATIVE_TOTAL_COST = "native_total_cost";
    private static final String KEY_FOREIGN_TOTAL_COST = "foreign_total_cost";
    private static final String KEY_AVG_COST_PER_PERSON = "average_cost_per_person";

    private ExpenseTable expenseTable;

    public JourneyTable(Context context)
    {
        this.mContext = context;
        this.dbHelper = DBHelper.getHelper(mContext);
        expenseTable = new ExpenseTable(context);
    }

    public static String createTable()
    {
        return "CREATE TABLE " + TABLE_NAME + " (" +
                KEY_ID + PRIMARY_KEY + NOT_NULL + COMMA_SEP +
                KEY_NAME + TEXT_TYPE + COMMA_SEP +
                KEY_DESTINATION + TEXT_TYPE + COMMA_SEP +
                KEY_CURRENCY + TEXT_TYPE + COMMA_SEP +
                KEY_NUM_OF_PEOPLE + INTEGER_TYPE + COMMA_SEP +
                KEY_NATIVE_TOTAL_COST + REAL_TYPE + COMMA_SEP +
                KEY_FOREIGN_TOTAL_COST + REAL_TYPE + COMMA_SEP +
                KEY_EXCHANGE_RATE + REAL_TYPE + COMMA_SEP +
                KEY_AVG_COST_PER_PERSON + REAL_TYPE + COMMA_SEP +
                KEY_START_DATE + DATETIME_TYPE + COMMA_SEP +
                KEY_END_DATE + DATETIME_TYPE + ");";
    }

    public int insert(String name, String destination, String currency, String startDate, String endDate, int numberOfPeople, double exchangeRate)
    {
        int journeyId;
        openDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME, name);
        values.put(KEY_DESTINATION, destination);
        values.put(KEY_CURRENCY, currency);
        values.put(KEY_START_DATE, startDate);
        values.put(KEY_END_DATE, endDate);
        values.put(KEY_NUM_OF_PEOPLE, numberOfPeople);
        values.put(KEY_FOREIGN_TOTAL_COST, 0);
        values.put(KEY_AVG_COST_PER_PERSON, 0);
        values.put(KEY_EXCHANGE_RATE, exchangeRate);

        journeyId = (int)database.insert(TABLE_NAME, null, values);

        closeDatabase();

        return journeyId;
    }

    public List<Journey> getAllJourneys()
    {
        List<Journey> journeys = new ArrayList<>();
        openDatabase();

        String selectQuery = "SELECT * FROM " + TABLE_NAME;

        Log.d(TAG, selectQuery);

        Cursor cursor = database.rawQuery(selectQuery, null);

        if (cursor != null && cursor.moveToFirst())
        {
            do
            {
                Journey journey = new Journey();

                journey.setId(cursor.getInt(cursor.getColumnIndex(KEY_ID)));
                journey.setName(cursor.getString(cursor.getColumnIndex(KEY_NAME)));
                journey.setDestination(cursor.getString(cursor.getColumnIndex(KEY_DESTINATION)));
                journey.setCurrency(cursor.getString(cursor.getColumnIndex(KEY_CURRENCY)));
                journey.setStartDate(cursor.getString(cursor.getColumnIndex(KEY_START_DATE)));
                journey.setEndDate(cursor.getString(cursor.getColumnIndex(KEY_END_DATE)));
                journey.setNumberOfPeople(cursor.getInt(cursor.getColumnIndex(KEY_NUM_OF_PEOPLE)));
                journey.setExchangeRate(cursor.getDouble(cursor.getColumnIndex(KEY_EXCHANGE_RATE)));

                journeys.add(journey);
            } while (cursor.moveToNext());
        }

        cursor.close();

        closeDatabase();

        return journeys;
    }

    public Journey getJourneyById(int id)
    {
        openDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_NAME + " WHERE id = " + id;

        Log.d(TAG, selectQuery);

        Journey journey = new Journey();;

        Cursor cursor = database.rawQuery(selectQuery, null);

        if (cursor != null && cursor.moveToFirst())
        {
            do
            {
                journey.setId(cursor.getInt(cursor.getColumnIndex(KEY_ID)));
                journey.setName(cursor.getString(cursor.getColumnIndex(KEY_NAME)));
                journey.setDestination(cursor.getString(cursor.getColumnIndex(KEY_DESTINATION)));
                journey.setCurrency(cursor.getString(cursor.getColumnIndex(KEY_CURRENCY)));
                journey.setStartDate(cursor.getString(cursor.getColumnIndex(KEY_START_DATE)));
                journey.setEndDate(cursor.getString(cursor.getColumnIndex(KEY_END_DATE)));
                journey.setNumberOfPeople(cursor.getInt(cursor.getColumnIndex(KEY_NUM_OF_PEOPLE)));
                journey.setExchangeRate(cursor.getDouble(cursor.getColumnIndex(KEY_EXCHANGE_RATE)));



            } while (cursor.moveToNext());
        }

        cursor.close();

        closeDatabase();

        List<Expense> expenses = expenseTable.getExpensesByJourney(journey.getId());

        double foreignTotal = 0;
        double nativeTotal = 0;
        for (Expense expense : expenses)
        {
            if (expense.getCurrency().equals("AUD"))
            {
                nativeTotal += expense.getAmount();
            }
            else
            {
                foreignTotal += expense.getAmount();
            }
        }

        journey.setNativeTotalCost(nativeTotal);
        journey.setForeignTotalCost(foreignTotal);

        return journey;
    }

}
