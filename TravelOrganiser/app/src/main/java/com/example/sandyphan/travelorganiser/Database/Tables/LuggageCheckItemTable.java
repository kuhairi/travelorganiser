package com.example.sandyphan.travelorganiser.Database.Tables;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.example.sandyphan.travelorganiser.Database.DBHelper;
import com.example.sandyphan.travelorganiser.Objects.LuggageCheckItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sandy.Phan on 02/03/2017.
 */

public class LuggageCheckItemTable extends BaseTable {

    private final static String TAG = LuggageCheckItemTable.class.getSimpleName();

    private static final String TABLE_NAME = "luggage_check_item";
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";
    private static final String KEY_IS_PACKED = "is_packed";
    private static final String KEY_JOURNEY_ID = "journey_id";

    public LuggageCheckItemTable(Context context) {
        super();
        this.mContext = context;
        this.dbHelper = DBHelper.getHelper(mContext);
    }

    public static String createTable()
    {
        return "CREATE TABLE " + TABLE_NAME + " (" +
                KEY_ID + PRIMARY_KEY + NOT_NULL + COMMA_SEP +
                KEY_NAME + TEXT_TYPE + NOT_NULL + COMMA_SEP +
                KEY_IS_PACKED + INTEGER_TYPE + NOT_NULL + COMMA_SEP +
                KEY_JOURNEY_ID + INTEGER_TYPE + NOT_NULL + COMMA_SEP +
                "FOREIGN KEY(" + KEY_JOURNEY_ID + ") REFERENCES journey(id));";
    }

    public List<LuggageCheckItem> getAllLuggageListItems(int journeyId)
    {
        String selectQuery = "SELECT * FROM " + TABLE_NAME +
                " WHERE " + KEY_JOURNEY_ID + " = " + journeyId;
        List<LuggageCheckItem> luggageCheckItems = new ArrayList<>();

        openDatabase();

        Log.e(TAG, selectQuery);

        Cursor cursor = database.rawQuery(selectQuery, null);

        if (cursor != null && cursor.moveToFirst())
        {
            do
            {
                LuggageCheckItem luggageCheckItem = new LuggageCheckItem();
                luggageCheckItem.setId(cursor.getInt(cursor.getColumnIndex(KEY_ID)));
                luggageCheckItem.setName(cursor.getString(cursor.getColumnIndex(KEY_NAME)));
                luggageCheckItem.setIsPacked(cursor.getInt(cursor.getColumnIndex(KEY_IS_PACKED)));

                luggageCheckItems.add(luggageCheckItem);
            } while (cursor.moveToNext());
        }

        cursor.close();

        closeDatabase();

        return luggageCheckItems;
    }

    public int insert(String name, int isPacked, int journeyId)
    {
        int luggageItemId;
        openDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME, name);
        values.put(KEY_JOURNEY_ID, journeyId);
        values.put(KEY_IS_PACKED, isPacked);

        luggageItemId = (int) database.insert(TABLE_NAME, null, values);
        closeDatabase();

        return luggageItemId;
    }

    public void updatePackingStatus(LuggageCheckItem luggageCheckItem, int isPacked)
    {
        ContentValues cv = new ContentValues();
        cv.put(KEY_IS_PACKED, isPacked);

        openDatabase();

        database.update(TABLE_NAME, cv, "id = " + luggageCheckItem.getId(), null);

        closeDatabase();
    }

    public void updateItemDescription(LuggageCheckItem luggageCheckItem, String description)
    {
        ContentValues cv = new ContentValues();
        cv.put(KEY_NAME, description);

        openDatabase();

        database.update(TABLE_NAME, cv, "id = " + luggageCheckItem.getId(), null);

        closeDatabase();
    }

    public void deleteItem(LuggageCheckItem luggageCheckItem)
    {
        openDatabase();

        String query = "DELETE FROM " + TABLE_NAME + " WHERE " + KEY_ID + " = " + luggageCheckItem.getId();
        database.execSQL(query);

        closeDatabase();

    }
}
