package com.example.sandyphan.travelorganiser.Activities;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ListView;

import com.example.sandyphan.travelorganiser.Activities.Base.BaseDetailsActivity;
import com.example.sandyphan.travelorganiser.Adapters.ExpensesAdapter;
import com.example.sandyphan.travelorganiser.Database.Tables.ExpenseTable;
import com.example.sandyphan.travelorganiser.Objects.Contants;
import com.example.sandyphan.travelorganiser.Objects.Expense;
import com.example.sandyphan.travelorganiser.R;

import java.util.List;

public class MyExpensesActivity extends BaseDetailsActivity {

    private final static String TAG = MyExpensesActivity.class.getSimpleName();

//    private RecyclerView recyclerView;
    private ListView expenseListView;
    private ExpensesAdapter expensesAdapter;
    private List<Expense> expenseList;

    private ExpenseTable expenseTable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expenses);
        super.onCreateBaseDetailsActivity();
        setToolbarTitle(Contants.MY_EXPENSE_TOOLBAR_TITLE);

        getJourneyIdFromExtras();

        expenseTable = new ExpenseTable(this);

        FloatingActionButton newExpenseButton = (FloatingActionButton) findViewById(R.id.add_expense_button);
        newExpenseButton.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View view) {
                openNewExpenseActivity(view);
            }
        });

//        recyclerView = (RecyclerView) findViewById(R.id.expense_recycler_view);

        expenseListView = (ListView) findViewById(R.id.expense_list_view);
        expenseList = expenseTable.getExpensesByJourney(1);
        expensesAdapter = new ExpensesAdapter(this, expenseList);

        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this, 1);
        expenseListView.setAdapter(expensesAdapter);
//        recyclerView.setLayoutManager(layoutManager);
//        recyclerView.setAdapter(expensesAdapter);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goBack();
            }
        });

    }

    public void openNewExpenseActivity(View view)
    {
        Intent intent = new Intent(this, NewExpenseActivity.class);
        intent.putExtra("JOURNEY_ID", journeyId);
        finish();
        startActivity(intent);
    }

    @Override
    public void onBackPressed()
    {
        goBack();
    }

    private void goBack()
    {
        Intent intent = new Intent(this, JourneyDetailsActivity.class);
        intent.putExtra("JOURNEY_ID", journeyId);
        finish();
        startActivity(intent);
    }

    private void getJourneyIdFromExtras()
    {
        Bundle extras = getIntent().getExtras();
        if (extras != null)
        {
            journeyId = extras.getInt("JOURNEY_ID");
        }
    }
}
