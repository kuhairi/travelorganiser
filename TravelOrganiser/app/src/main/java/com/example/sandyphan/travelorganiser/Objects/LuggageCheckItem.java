package com.example.sandyphan.travelorganiser.Objects;

/**
 * Created by Sandy.Phan on 02/03/2017.
 */

public class LuggageCheckItem extends TravelObject {
    private final static String TAG = LuggageCheckItem.class.getSimpleName();

    private int _journeyId;
    private int _isPacked;

    public LuggageCheckItem() { super(); }

    public int getIsPacked() {
        return _isPacked;
    }

    public void setIsPacked(int _isPacked) {
        this._isPacked = _isPacked;
    }

    public int getJourneyId() {
        return _journeyId;
    }

    public void setJourneyId(int _journeyId) {
        this._journeyId = _journeyId;
    }

}
